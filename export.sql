--------------------------------------------------------
--  File created - Saturday-February-24-2018   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for View ALL_BOOK_IN_VEH
--------------------------------------------------------

  CREATE OR REPLACE FORCE VIEW "FUADRAFID"."ALL_BOOK_IN_VEH" ("ISBN_NO", "BOOKS_NAME", "AUTHOR", "CATEGORY", "EDITION", "PRICE", "TYPE", "REG_NO") AS 
  Select DISTINCT ISBN_NO,BOOKS_NAME,name,books.CATEGORY,EDITION,PRICE,TYPE,reg_no from books natural join WRITTEN_BY natural join AUTHOR natural join book_on_vehicle

;
--------------------------------------------------------
--  DDL for View BOOK_AUTHOR_VIEW
--------------------------------------------------------

  CREATE OR REPLACE FORCE VIEW "FUADRAFID"."BOOK_AUTHOR_VIEW" ("AuthID", "Book Name", "Category", "Edition", "Price", "Type", "Author", "Exist") AS 
  select Author_ID,Books_name,category, edition, price, type, name,AVAILABILITY
from books natural join written_by natural join author

;
--------------------------------------------------------
--  DDL for View BOOK_BOOKSTORE_VIEW
--------------------------------------------------------

  CREATE OR REPLACE FORCE VIEW "FUADRAFID"."BOOK_BOOKSTORE_VIEW" ("Book Name", "Category", "Edition", "Price", "Type", "Bought From", "Store_ID") AS 
  select Books_name, category, edition, price, type, name,store_id
from books natural join supplied_by natural join book_store

;
--------------------------------------------------------
--  DDL for View BOOK_LIST_VIEW
--------------------------------------------------------

  CREATE OR REPLACE FORCE VIEW "FUADRAFID"."BOOK_LIST_VIEW" ("ISBN NO", "Book Name", "Category", "Edition", "Price", "Type", "Author_name", "Publisher ", "Bought From") AS 
  select isbn_no,Books_name, category, edition, price, type, author.name, publisher.name, book_store.name
from publisher join published_by using (p_reg_no) join books using (isbn_no) join written_by using (isbn_no) join author using(author_id) join supplied_by using (isbn_no) join book_store using(store_id)

;
--------------------------------------------------------
--  DDL for View BOOK_PUBLISHER_VIEW
--------------------------------------------------------

  CREATE OR REPLACE FORCE VIEW "FUADRAFID"."BOOK_PUBLISHER_VIEW" ("Book Name", "Category", "Edition", "Price", "Type", "Publisher", "Pub_id") AS 
  select Books_name, category, edition, price, type, name,P_REG_NO
from books natural join published_by natural join publisher

;
--------------------------------------------------------
--  DDL for View BOOK_REQS
--------------------------------------------------------

  CREATE OR REPLACE FORCE VIEW "FUADRAFID"."BOOK_REQS" ("BOOK", "AUTHOR", "EDITION", "CATEGORY", "ID") AS 
  SELECT FUADRAFID.BOOKS.BOOKS_NAME,NAME,EDITION,CATEGORY,MEMBER_ID FROM FUADRAFID.BOOKS NATURAL JOIN FUADRAFID.REQUESTS_FOR NATURAL JOIN WRITTEN_BY NATURAL JOIN AUTHOR

;
--------------------------------------------------------
--  DDL for View BOOK_SPEND_MONEY
--------------------------------------------------------

  CREATE OR REPLACE FORCE VIEW "FUADRAFID"."BOOK_SPEND_MONEY" ("RECEIPT NO", "AMOUNT", "EXPENSE DATE", "BOOK", "SUPPLIER", "QUANTITY") AS 
  select RECEIPT_NO,AMOUNT,EXPENSE_DATE,BOOKS_NAME,NAME,QUANTITY from expense natural join BOOKS_BOUGHT NATURAL JOIN books  NATURAl JOIN SUPPLIED_BY NATURAL JOIN BOOK_STORE

;
--------------------------------------------------------
--  DDL for View BOOK_VEHICLE
--------------------------------------------------------

  CREATE OR REPLACE FORCE VIEW "FUADRAFID"."BOOK_VEHICLE" ("BOOK", "AUTHOR", "CATEGORY", "EDITION", "MEMBER_ID", "AUTHOR_ID", "BOOK_ISBN") AS 
  SELECT DISTINCT BOOKS_NAME,NAME,CATEGORY,EDITION,MEMBER_ID,AUTHOR_ID,ISBN_NO FROM FUADRAFID.AUTHOR NATURAL JOIN 
                FUADRAFID.WRITTEN_BY NATURAL JOIN FUADRAFID.BOOKS NATURAL JOIN BOOK_SERIAL
                NATURAL JOIN FUADRAFID.BOOK_ON_VEHICLE NATURAL JOIN FUADRAFID.MEMBER_ON_VEHICLE

;
--------------------------------------------------------
--  DDL for View BOOK_VEHICLE_EMP
--------------------------------------------------------

  CREATE OR REPLACE FORCE VIEW "FUADRAFID"."BOOK_VEHICLE_EMP" ("BOOK_ID", "BOOK", "AUTHOR", "CATEGORY", "EDITION", "ID", "AUTHOR_ID", "BOOK_ISBN") AS 
  SELECT DISTINCT ISBN_NO,BOOKS_NAME,NAME,CATEGORY,EDITION,EMP_ID,AUTHOR_ID,ISBN_NO FROM FUADRAFID.AUTHOR NATURAL JOIN 
                FUADRAFID.WRITTEN_BY NATURAL JOIN FUADRAFID.BOOKS NATURAL JOIN BOOK_SERIAL
                NATURAL JOIN FUADRAFID.BOOK_ON_VEHICLE NATURAL JOIN FUADRAFID.HANDLES

;
--------------------------------------------------------
--  DDL for View CULTURAL_PROGRAM_EXPENSE
--------------------------------------------------------

  CREATE OR REPLACE FORCE VIEW "FUADRAFID"."CULTURAL_PROGRAM_EXPENSE" ("RECEIPT NO", "AMOUNT", "EXPENSE_DATE", "PROG NO", "LOCATION", "PROG DATE", "DESCRIPTION", "ARRANGED BY") AS 
  select "RECEIPT_NO", "AMOUNT","EXPENSE_DATE","PROG_NO","LOCATION","PROG_DATE","DESCRIPTION","EMP_ID" from expense natural join program_expense natural join cultural_program NATURAL JOIN ARRANGE

;
--------------------------------------------------------
--  DDL for View CULTURAL_PROG_VIEW
--------------------------------------------------------

  CREATE OR REPLACE FORCE VIEW "FUADRAFID"."CULTURAL_PROG_VIEW" ("Employee In Charge", "Program", "Location", "Date", "Program ID") AS 
  select emp_id, description, location, prog_date,PROG_NO
from cultural_program natural join arrange natural join employee

;
--------------------------------------------------------
--  DDL for View DONATIONS_GIVEN
--------------------------------------------------------

  CREATE OR REPLACE FORCE VIEW "FUADRAFID"."DONATIONS_GIVEN" ("SERIAL NO", "AMOUNT", "TRANSACTION DATE", "DONATOR", "ADDRESS", "CONTACT NO", "BANK ACC NO") AS 
  select "SERIAL_NO","AMOUNT","TRANSAC_DATE","DONATOR_NAME","ADDRESS","CONTACT_NO","BANK_ACC_NO" from income natural join donation natural join donators

;
--------------------------------------------------------
--  DDL for View DRIVER_APPLICANTS_VIEW
--------------------------------------------------------

  CREATE OR REPLACE FORCE VIEW "FUADRAFID"."DRIVER_APPLICANTS_VIEW" ("Employee In Charge", "Applicantion No", "Applicant Name", "Email", "Contact No.", "Gender", "License No.", "Police Record", "Previous Experiences") AS 
  select emp_id,application_no, name, job_applicants.email, job_applicants.contact_no, job_applicants.gender, license_no, police_record, experience
from employee join handles_application using (emp_id) join job_applicants using(application_no) join driver_applicants using(application_no)

;
--------------------------------------------------------
--  DDL for View LIBRARIAN_APPLICANTS_VIEW
--------------------------------------------------------

  CREATE OR REPLACE FORCE VIEW "FUADRAFID"."LIBRARIAN_APPLICANTS_VIEW" ("Employee In Charge", "Applicantion No", "Applicant Name", "Email", "Contact No.", "Gender", "Exam Result", "Specialization", "Previous Experiences") AS 
  select emp_id,application_no, name, job_applicants.email, job_applicants.contact_no, job_applicants.gender, exam_result, specialization, experience
from employee join handles_application using (emp_id) join job_applicants using(application_no) join librarian_applicants using(application_no)

;
--------------------------------------------------------
--  DDL for View LIB_EMP
--------------------------------------------------------

  CREATE OR REPLACE FORCE VIEW "FUADRAFID"."LIB_EMP" ("ID", "NAME", "GENDER", "REG_NO") AS 
  SELECT EMP_ID,NAME_ENGLISH,GENDER,REG_NO FROM EMPLOYEE NATURAL JOIN LOGIN_LIBRARIANS NATURAL JOIN HANDLES

;
--------------------------------------------------------
--  DDL for View LIB_MAINT
--------------------------------------------------------

  CREATE OR REPLACE FORCE VIEW "FUADRAFID"."LIB_MAINT" ("REG_NO", "RECEIPT_NO", "DESCRIPTION", "DATE_DONE", "NEXT_DUE_DATE", "ID", "AMOUNT", "EXPENSE_ENTRY_DATE") AS 
  select REG_NO,RECEIPT_NO,DESCRIPTION,DATE_DONE,NEXT_DUE_DATE,EMP_ID,AMOUNT,EXPENSE_DATE
  
  from expense natural join maintainance_cost natural join maintainance natural join HANDLES

;
--------------------------------------------------------
--  DDL for View LIB_MEM
--------------------------------------------------------

  CREATE OR REPLACE FORCE VIEW "FUADRAFID"."LIB_MEM" ("ID", "NAME", "CONTACT_NO", "EMAIL_ADDRESS", "GENDER", "JOINING_DATE", "MEMBERSHIP_TYPE", "LIB_ID", "REG", "USERNAME", "PASS") AS 
  select member_id,member_name,contact_no,email_address,gender,joining_date,membership_type,emp_id,reg_no,username,password from login_members natural join members natural join MEMBER_ON_VEHICLE natural join HANDLES

;
--------------------------------------------------------
--  DDL for View LIB_VEH_COMP
--------------------------------------------------------

  CREATE OR REPLACE FORCE VIEW "FUADRAFID"."LIB_VEH_COMP" ("SERIAL_NO", "DESCRIPTION", "STATUS", "DATE_OF_ADDITION", "TYPE", "REG_NO") AS 
  Select SERIAL_NO,DESCRIPTION,STATUS,DATE_OF_ADDITION,TYPE,REG_NO  from fuadrafid.components natural join fuadrafid.vehicle_components

;
--------------------------------------------------------
--  DDL for View LOG_LIB
--------------------------------------------------------

  CREATE OR REPLACE FORCE VIEW "FUADRAFID"."LOG_LIB" ("USERNAME", "EMP_ID") AS 
  select username,emp_id from login_librarians

;
--------------------------------------------------------
--  DDL for View LOG_MEM
--------------------------------------------------------

  CREATE OR REPLACE FORCE VIEW "FUADRAFID"."LOG_MEM" ("USERNAME", "MEMBER_ID") AS 
  select username,member_id from LOGIN_MEMBERS

;
--------------------------------------------------------
--  DDL for View MEM_DEP
--------------------------------------------------------

  CREATE OR REPLACE FORCE VIEW "FUADRAFID"."MEM_DEP" ("SERIAL_NO", "AMOUNT", "TRANSAC_DATE", "MEMBER_NAME", "REG") AS 
  select SERIAL_NO,AMOUNT,TRANSAC_DATE,MEMBER_NAME,REG_NO from income natural join 
MEMBER_DEPOSITIONS natural join members natural join member_on_vehicle

;
--------------------------------------------------------
--  DDL for View NEW_COMPONENT
--------------------------------------------------------

  CREATE OR REPLACE FORCE VIEW "FUADRAFID"."NEW_COMPONENT" ("RECEIPT_NO", "VEHICLE REG NO", "COMPONENT SERIAL NO", "AMOUNT", "EXPENSE DATE", "DESCRIPTION", "STATUS", "DATE OF ADDITION", "TYPE", "SERVICE START DATE", "MODEL") AS 
  select "RECEIPT_NO", "REG_NO","SERIAL_NO","AMOUNT","EXPENSE_DATE","DESCRIPTION","STATUS","DATE_OF_ADDITION","TYPE","SERVICE_START_DATE","MODEL" from EXPENSE natural join COMPONENTS_BOUGHT NATURAL join COMPONENTS natural join VEHICLE_COMPONENTS natural join VEHICLE

;
--------------------------------------------------------
--  DDL for View NEW_VEHICLE
--------------------------------------------------------

  CREATE OR REPLACE FORCE VIEW "FUADRAFID"."NEW_VEHICLE" ("RECEIPT NO", "VEHICLE REG NO", "AMOUNT", "EXPENSE DATE", "SERVICE START DATE", "MODEL") AS 
  select "RECEIPT_NO","REG_NO","AMOUNT","EXPENSE_DATE","SERVICE_START_DATE","MODEL" from expense natural join vehicle_bought natural join vehicle

;
--------------------------------------------------------
--  DDL for View OFFICER_APPLICANTS_VIEW
--------------------------------------------------------

  CREATE OR REPLACE FORCE VIEW "FUADRAFID"."OFFICER_APPLICANTS_VIEW" ("Employee In Charge", "Applicantion No", "Applicant Name", "Email", "Contact No.", "Gender", "Exam Result", "Computer Skills", "Previous Experiences") AS 
  select emp_id, application_no, name, job_applicants.email, job_applicants.contact_no, job_applicants.gender, exam_result, computer_skills, experience
from employee join handles_application using (emp_id) join job_applicants using(application_no) join officer_applicants using(application_no)

;
--------------------------------------------------------
--  DDL for View OFFICER_BOOK_REQS
--------------------------------------------------------

  CREATE OR REPLACE FORCE VIEW "FUADRAFID"."OFFICER_BOOK_REQS" ("BOOK", "AUTHOR", "EDITION", "CATEGORY", "REG NO") AS 
  SELECT FUADRAFID.BOOKS.BOOKS_NAME,NAME,EDITION,CATEGORY,REG_NO FROM FUADRAFID.MEMBER_ON_VEHICLE NATURAL JOIN  FUADRAFID.REQUESTS_FOR NATURAL JOIN FUADRAFID.BOOKS NATURAL JOIN WRITTEN_BY NATURAL JOIN AUTHOR

;
--------------------------------------------------------
--  DDL for View OFF_EMP
--------------------------------------------------------

  CREATE OR REPLACE FORCE VIEW "FUADRAFID"."OFF_EMP" ("ID", "NAME", "GENDER", "REG_NO") AS 
  SELECT EMP_ID,NAME_ENGLISH,GENDER,REG_NO FROM EMPLOYEE NATURAL JOIN LOGIN_OFFICERS NATURAL JOIN HANDLES

;
--------------------------------------------------------
--  DDL for View PRIZE_WINNER_VIEW
--------------------------------------------------------

  CREATE OR REPLACE FORCE VIEW "FUADRAFID"."PRIZE_WINNER_VIEW" ("Employee In Charge", "Winner Name", "Contact No.", "Category", "Position", "Program Of", "Location", "Date", "Prog No") AS 
  select emp_id, win_name, prize_winners.contact_no, category, position, description, location, prog_date,PROG_NO
from employee join arrange using (emp_id) join cultural_program using(prog_no) join prize_winners using(prog_no)

;
--------------------------------------------------------
--  DDL for View VEHICLE_BOOK_SERIAL
--------------------------------------------------------

  CREATE OR REPLACE FORCE VIEW "FUADRAFID"."VEHICLE_BOOK_SERIAL" ("SERIAL_NO", "ISBN_NO", "EMP_ID") AS 
  select  serial_no,isbn_no,emp_id from book_serial natural join BOOK_ON_VEHICLE natural join Handles

;
--------------------------------------------------------
--  DDL for View VEHICLE_MAINTAINANCE
--------------------------------------------------------

  CREATE OR REPLACE FORCE VIEW "FUADRAFID"."VEHICLE_MAINTAINANCE" ("RECEIPT NO", "VEHICLE REG NO", "MAINTAINANCE SERIAL NO", "DESCRIPTION", "DATE DONE", "NEXT DUE DATE", "AMOUNT", "EXPENSE DATE") AS 
  select "RECEIPT_NO","REG_NO","SERIAL_NO","DESCRIPTION","DATE_DONE","NEXT_DUE_DATE","AMOUNT","EXPENSE_DATE" from vehicle natural join MAINTAINANCE natural join MAINTAINANCE_COST natural join expense

;
--------------------------------------------------------
--  DDL for Table ARRANGE
--------------------------------------------------------

  CREATE TABLE "FUADRAFID"."ARRANGE" 
   (	"EMP_ID" VARCHAR2(50), 
	"PROG_NO" VARCHAR2(50)
   ) ;
--------------------------------------------------------
--  DDL for Table AUTHOR
--------------------------------------------------------

  CREATE TABLE "FUADRAFID"."AUTHOR" 
   (	"AUTHOR_ID" VARCHAR2(50), 
	"NAME" VARCHAR2(50), 
	"WRITING_CATEGORY" VARCHAR2(50), 
	"VERIFIED" VARCHAR2(3)
   ) ;
--------------------------------------------------------
--  DDL for Table BOOKS
--------------------------------------------------------

  CREATE TABLE "FUADRAFID"."BOOKS" 
   (	"ISBN_NO" VARCHAR2(50), 
	"BOOKS_NAME" NVARCHAR2(50), 
	"AVAILABILITY" NUMBER(20,0), 
	"CATEGORY" VARCHAR2(50), 
	"EDITION" VARCHAR2(50), 
	"PRICE" NUMBER(20,0), 
	"TYPE" VARCHAR2(50)
   ) ;
--------------------------------------------------------
--  DDL for Table BOOKS_BOUGHT
--------------------------------------------------------

  CREATE TABLE "FUADRAFID"."BOOKS_BOUGHT" 
   (	"ISBN_NO" VARCHAR2(50), 
	"RECEIPT_NO" VARCHAR2(50), 
	"QUANTITY" VARCHAR2(50)
   ) ;
--------------------------------------------------------
--  DDL for Table BOOK_ON_VEHICLE
--------------------------------------------------------

  CREATE TABLE "FUADRAFID"."BOOK_ON_VEHICLE" 
   (	"ISBN_NO" VARCHAR2(50), 
	"SERIAL_NO" VARCHAR2(50), 
	"REG_NO" VARCHAR2(50)
   ) ;
--------------------------------------------------------
--  DDL for Table BOOK_SERIAL
--------------------------------------------------------

  CREATE TABLE "FUADRAFID"."BOOK_SERIAL" 
   (	"ISBN_NO" VARCHAR2(50), 
	"SERIAL_NO" VARCHAR2(50)
   ) ;
--------------------------------------------------------
--  DDL for Table BOOK_STORE
--------------------------------------------------------

  CREATE TABLE "FUADRAFID"."BOOK_STORE" 
   (	"STORE_ID" VARCHAR2(50), 
	"NAME" VARCHAR2(50), 
	"ADDRESS" VARCHAR2(50), 
	"CONTACT_NO" VARCHAR2(11)
   ) ;
--------------------------------------------------------
--  DDL for Table COMPONENTS
--------------------------------------------------------

  CREATE TABLE "FUADRAFID"."COMPONENTS" 
   (	"SERIAL_NO" VARCHAR2(50), 
	"DESCRIPTION" VARCHAR2(50), 
	"STATUS" VARCHAR2(20), 
	"DATE_OF_ADDITION" DATE, 
	"TYPE" VARCHAR2(50)
   ) ;
--------------------------------------------------------
--  DDL for Table COMPONENTS_BOUGHT
--------------------------------------------------------

  CREATE TABLE "FUADRAFID"."COMPONENTS_BOUGHT" 
   (	"RECEIPT_NO" VARCHAR2(50), 
	"SERIAL_NO" VARCHAR2(50)
   ) ;
--------------------------------------------------------
--  DDL for Table CULTURAL_PROGRAM
--------------------------------------------------------

  CREATE TABLE "FUADRAFID"."CULTURAL_PROGRAM" 
   (	"PROG_NO" VARCHAR2(50), 
	"LOCATION" VARCHAR2(50), 
	"PROG_DATE" DATE, 
	"DESCRIPTION" NVARCHAR2(50)
   ) ;
--------------------------------------------------------
--  DDL for Table DONATION
--------------------------------------------------------

  CREATE TABLE "FUADRAFID"."DONATION" 
   (	"SERIAL_NO" VARCHAR2(50), 
	"DONATOR_ID" VARCHAR2(50)
   ) ;
--------------------------------------------------------
--  DDL for Table DONATORS
--------------------------------------------------------

  CREATE TABLE "FUADRAFID"."DONATORS" 
   (	"DONATOR_ID" VARCHAR2(50), 
	"DONATOR_NAME" VARCHAR2(50), 
	"ADDRESS" VARCHAR2(50), 
	"CONTACT_NO" VARCHAR2(20), 
	"BANK_ACC_NO" VARCHAR2(20)
   ) ;
--------------------------------------------------------
--  DDL for Table DRIVER
--------------------------------------------------------

  CREATE TABLE "FUADRAFID"."DRIVER" 
   (	"EMP_ID" VARCHAR2(50), 
	"LICENSE_NO" VARCHAR2(50), 
	"DRIVING_EXPERIENCE" VARCHAR2(50), 
	"WORKING_PLACE" VARCHAR2(50), 
	"DRIVING_RECORD" VARCHAR2(50)
   ) ;
--------------------------------------------------------
--  DDL for Table DRIVER_APPLICANTS
--------------------------------------------------------

  CREATE TABLE "FUADRAFID"."DRIVER_APPLICANTS" 
   (	"APPLICATION_NO" VARCHAR2(50), 
	"LICENSE_NO" VARCHAR2(50), 
	"POLICE_RECORD" VARCHAR2(50)
   ) ;
--------------------------------------------------------
--  DDL for Table EDUCATIONAL_QUALIFICATION
--------------------------------------------------------

  CREATE TABLE "FUADRAFID"."EDUCATIONAL_QUALIFICATION" 
   (	"EMP_ID" VARCHAR2(50), 
	"NAME" VARCHAR2(50), 
	"INSTITUTE" VARCHAR2(50), 
	"BOARD" VARCHAR2(50), 
	"GPA" NUMBER(3,2)
   ) ;
--------------------------------------------------------
--  DDL for Table EDU_QUAL_APP
--------------------------------------------------------

  CREATE TABLE "FUADRAFID"."EDU_QUAL_APP" 
   (	"APPLICATION_NO" VARCHAR2(50), 
	"NAME" VARCHAR2(50), 
	"INSTIUTE" VARCHAR2(50), 
	"BOARD" VARCHAR2(50), 
	"GPA" VARCHAR2(50), 
	"PASSING_YEAR" DATE
   ) ;
--------------------------------------------------------
--  DDL for Table EMPLOYEE
--------------------------------------------------------

  CREATE TABLE "FUADRAFID"."EMPLOYEE" 
   (	"EMP_ID" VARCHAR2(50), 
	"NAME_BANGLA" NVARCHAR2(50), 
	"NAME_ENGLISH" VARCHAR2(50), 
	"ADDRESS" VARCHAR2(50), 
	"DATE_OF_BIRTH" DATE, 
	"CONTACT_NO" VARCHAR2(20), 
	"EMAIL" VARCHAR2(50), 
	"MARITAL_STATUS" VARCHAR2(50), 
	"DATE_OF_JOINING" DATE, 
	"SALARY" NUMBER(20,0), 
	"GENDER" VARCHAR2(10)
   ) ;
--------------------------------------------------------
--  DDL for Table EXPENSE
--------------------------------------------------------

  CREATE TABLE "FUADRAFID"."EXPENSE" 
   (	"RECEIPT_NO" VARCHAR2(50), 
	"AMOUNT" NUMBER(20,0), 
	"EXPENSE_DATE" DATE
   ) ;
--------------------------------------------------------
--  DDL for Table HAD_ISSUE
--------------------------------------------------------

  CREATE TABLE "FUADRAFID"."HAD_ISSUE" 
   (	"ISBN_NO" VARCHAR2(50), 
	"ISSUE_NO" VARCHAR2(50)
   ) ;
--------------------------------------------------------
--  DDL for Table HANDLES
--------------------------------------------------------

  CREATE TABLE "FUADRAFID"."HANDLES" 
   (	"EMP_ID" VARCHAR2(50), 
	"REG_NO" VARCHAR2(50), 
	"HANDLE_DATE" DATE
   ) ;
--------------------------------------------------------
--  DDL for Table HANDLES_APPLICATION
--------------------------------------------------------

  CREATE TABLE "FUADRAFID"."HANDLES_APPLICATION" 
   (	"APPLICATION_NO" VARCHAR2(50), 
	"EMP_ID" VARCHAR2(50)
   ) ;
--------------------------------------------------------
--  DDL for Table HAS_ISSUE
--------------------------------------------------------

  CREATE TABLE "FUADRAFID"."HAS_ISSUE" 
   (	"ISBN_NO" VARCHAR2(50), 
	"SERIAL_NO" VARCHAR2(50), 
	"ISSUE_NO" VARCHAR2(50)
   ) ;
--------------------------------------------------------
--  DDL for Table HAS_VOLUNTEERS
--------------------------------------------------------

  CREATE TABLE "FUADRAFID"."HAS_VOLUNTEERS" 
   (	"VOLUNTEER_ID" VARCHAR2(50), 
	"PROG_NO" VARCHAR2(50)
   ) ;
--------------------------------------------------------
--  DDL for Table HTMLDB_PLAN_TABLE
--------------------------------------------------------

  CREATE TABLE "FUADRAFID"."HTMLDB_PLAN_TABLE" 
   (	"STATEMENT_ID" VARCHAR2(30), 
	"PLAN_ID" NUMBER, 
	"TIMESTAMP" DATE, 
	"REMARKS" VARCHAR2(4000), 
	"OPERATION" VARCHAR2(30), 
	"OPTIONS" VARCHAR2(255), 
	"OBJECT_NODE" VARCHAR2(128), 
	"OBJECT_OWNER" VARCHAR2(30), 
	"OBJECT_NAME" VARCHAR2(30), 
	"OBJECT_ALIAS" VARCHAR2(65), 
	"OBJECT_INSTANCE" NUMBER(*,0), 
	"OBJECT_TYPE" VARCHAR2(30), 
	"OPTIMIZER" VARCHAR2(255), 
	"SEARCH_COLUMNS" NUMBER, 
	"ID" NUMBER(*,0), 
	"PARENT_ID" NUMBER(*,0), 
	"DEPTH" NUMBER(*,0), 
	"POSITION" NUMBER(*,0), 
	"COST" NUMBER(*,0), 
	"CARDINALITY" NUMBER(*,0), 
	"BYTES" NUMBER(*,0), 
	"OTHER_TAG" VARCHAR2(255), 
	"PARTITION_START" VARCHAR2(255), 
	"PARTITION_STOP" VARCHAR2(255), 
	"PARTITION_ID" NUMBER(*,0), 
	"OTHER" LONG, 
	"DISTRIBUTION" VARCHAR2(30), 
	"CPU_COST" NUMBER(*,0), 
	"IO_COST" NUMBER(*,0), 
	"TEMP_SPACE" NUMBER(*,0), 
	"ACCESS_PREDICATES" VARCHAR2(4000), 
	"FILTER_PREDICATES" VARCHAR2(4000), 
	"PROJECTION" VARCHAR2(4000), 
	"TIME" NUMBER(*,0), 
	"QBLOCK_NAME" VARCHAR2(30)
   ) ;
--------------------------------------------------------
--  DDL for Table INCOME
--------------------------------------------------------

  CREATE TABLE "FUADRAFID"."INCOME" 
   (	"SERIAL_NO" VARCHAR2(50), 
	"AMOUNT" NUMBER(*,0), 
	"TRANSAC_DATE" DATE
   ) ;
--------------------------------------------------------
--  DDL for Table ISSUE
--------------------------------------------------------

  CREATE TABLE "FUADRAFID"."ISSUE" 
   (	"ISSUE_NO" VARCHAR2(50), 
	"ISSUE_DATE" DATE, 
	"EXPIRY_DATE" DATE GENERATED ALWAYS AS ("ISSUE_DATE"+7) VIRTUAL VISIBLE , 
	"RETURN_DATE" DATE
   ) ;
--------------------------------------------------------
--  DDL for Table ISSUED_TO
--------------------------------------------------------

  CREATE TABLE "FUADRAFID"."ISSUED_TO" 
   (	"ISSUE_NO" VARCHAR2(50), 
	"MEMBER_ID" VARCHAR2(50)
   ) ;
--------------------------------------------------------
--  DDL for Table JOB_APPLICANTS
--------------------------------------------------------

  CREATE TABLE "FUADRAFID"."JOB_APPLICANTS" 
   (	"APPLICATION_NO" VARCHAR2(50), 
	"NAME" VARCHAR2(50), 
	"EMAIL" VARCHAR2(50), 
	"CONTACT_NO" VARCHAR2(20), 
	"GENDER" VARCHAR2(50), 
	"EXPERIENCE" VARCHAR2(50), 
	"DATE_OF_BIRTH" DATE
   ) ;
--------------------------------------------------------
--  DDL for Table LIBRARIAN
--------------------------------------------------------

  CREATE TABLE "FUADRAFID"."LIBRARIAN" 
   (	"EMP_ID" VARCHAR2(50), 
	"WORKING_PLACE" VARCHAR2(50), 
	"SPECIALIZATION" VARCHAR2(30)
   ) ;
--------------------------------------------------------
--  DDL for Table LIBRARIAN_APPLICANTS
--------------------------------------------------------

  CREATE TABLE "FUADRAFID"."LIBRARIAN_APPLICANTS" 
   (	"APPLICATION_NO" VARCHAR2(50), 
	"EXAM_RESULT" VARCHAR2(50), 
	"SPECIALIZATION" VARCHAR2(30)
   ) ;
--------------------------------------------------------
--  DDL for Table LOGIN_LIBRARIANS
--------------------------------------------------------

  CREATE TABLE "FUADRAFID"."LOGIN_LIBRARIANS" 
   (	"USERNAME" VARCHAR2(50), 
	"EMP_ID" VARCHAR2(50), 
	"PASSWORD" VARCHAR2(20)
   ) ;
--------------------------------------------------------
--  DDL for Table LOGIN_MEMBERS
--------------------------------------------------------

  CREATE TABLE "FUADRAFID"."LOGIN_MEMBERS" 
   (	"USERNAME" VARCHAR2(50), 
	"MEMBER_ID" VARCHAR2(50), 
	"PASSWORD" VARCHAR2(20)
   ) ;
--------------------------------------------------------
--  DDL for Table LOGIN_OFFICERS
--------------------------------------------------------

  CREATE TABLE "FUADRAFID"."LOGIN_OFFICERS" 
   (	"USERNAME" VARCHAR2(50), 
	"EMP_ID" VARCHAR2(50), 
	"PASSWORD" VARCHAR2(50)
   ) ;
--------------------------------------------------------
--  DDL for Table MAINTAINANCE
--------------------------------------------------------

  CREATE TABLE "FUADRAFID"."MAINTAINANCE" 
   (	"SERIAL_NO" VARCHAR2(50), 
	"REG_NO" VARCHAR2(50), 
	"DESCRIPTION" VARCHAR2(50), 
	"DATE_DONE" DATE, 
	"NEXT_DUE_DATE" DATE
   ) ;
--------------------------------------------------------
--  DDL for Table MAINTAINANCE_COST
--------------------------------------------------------

  CREATE TABLE "FUADRAFID"."MAINTAINANCE_COST" 
   (	"RECEIPT_NO" VARCHAR2(50), 
	"REG_NO" VARCHAR2(50), 
	"SERIAL_NO" VARCHAR2(50)
   ) ;
--------------------------------------------------------
--  DDL for Table MEMBERS
--------------------------------------------------------

  CREATE TABLE "FUADRAFID"."MEMBERS" 
   (	"MEMBER_ID" VARCHAR2(50), 
	"MEMBER_NAME" VARCHAR2(50), 
	"CONTACT_NO" VARCHAR2(50), 
	"EMAIL_ADDRESS" VARCHAR2(50), 
	"GENDER" VARCHAR2(50), 
	"JOINING_DATE" DATE, 
	"MEMBERSHIP_TYPE" VARCHAR2(50)
   ) ;
--------------------------------------------------------
--  DDL for Table MEMBER_DEPOSITIONS
--------------------------------------------------------

  CREATE TABLE "FUADRAFID"."MEMBER_DEPOSITIONS" 
   (	"MEMBER_ID" VARCHAR2(50), 
	"SERIAL_NO" VARCHAR2(50)
   ) ;
--------------------------------------------------------
--  DDL for Table MEMBER_ON_VEHICLE
--------------------------------------------------------

  CREATE TABLE "FUADRAFID"."MEMBER_ON_VEHICLE" 
   (	"MEMBER_ID" VARCHAR2(50), 
	"REG_NO" VARCHAR2(50)
   ) ;
--------------------------------------------------------
--  DDL for Table OFFICER
--------------------------------------------------------

  CREATE TABLE "FUADRAFID"."OFFICER" 
   (	"EMP_ID" VARCHAR2(50), 
	"DESIGNATION" VARCHAR2(50), 
	"DEPARTMENT" VARCHAR2(50)
   ) ;
--------------------------------------------------------
--  DDL for Table OFFICER_APPLICANTS
--------------------------------------------------------

  CREATE TABLE "FUADRAFID"."OFFICER_APPLICANTS" 
   (	"APPLICATION_NO" VARCHAR2(50), 
	"EXAM_RESULT" VARCHAR2(50), 
	"COMPUTER_SKILLS" VARCHAR2(50)
   ) ;
--------------------------------------------------------
--  DDL for Table PRIZE_WINNERS
--------------------------------------------------------

  CREATE TABLE "FUADRAFID"."PRIZE_WINNERS" 
   (	"PROG_NO" VARCHAR2(50), 
	"WIN_NAME" VARCHAR2(50), 
	"CONTACT_NO" VARCHAR2(50), 
	"CATEGORY" VARCHAR2(50), 
	"POSITION" NUMBER(20,0)
   ) ;
--------------------------------------------------------
--  DDL for Table PROGRAM_EXPENSE
--------------------------------------------------------

  CREATE TABLE "FUADRAFID"."PROGRAM_EXPENSE" 
   (	"RECEIPT_NO" VARCHAR2(50), 
	"PROG_NO" VARCHAR2(50)
   ) ;
--------------------------------------------------------
--  DDL for Table PUBLISHED_BY
--------------------------------------------------------

  CREATE TABLE "FUADRAFID"."PUBLISHED_BY" 
   (	"ISBN_NO" VARCHAR2(50), 
	"P_REG_NO" VARCHAR2(50)
   ) ;
--------------------------------------------------------
--  DDL for Table PUBLISHER
--------------------------------------------------------

  CREATE TABLE "FUADRAFID"."PUBLISHER" 
   (	"P_REG_NO" VARCHAR2(50), 
	"NAME" VARCHAR2(50), 
	"ADDRESS" VARCHAR2(50), 
	"CONTACT_NO" VARCHAR2(11)
   ) ;
--------------------------------------------------------
--  DDL for Table REQUESTS_FOR
--------------------------------------------------------

  CREATE TABLE "FUADRAFID"."REQUESTS_FOR" 
   (	"MEMBER_ID" VARCHAR2(50), 
	"ISBN_NO" VARCHAR2(50), 
	"REQ_DATE" DATE
   ) ;
--------------------------------------------------------
--  DDL for Table SALARY_GIVEN_TO
--------------------------------------------------------

  CREATE TABLE "FUADRAFID"."SALARY_GIVEN_TO" 
   (	"RECEIPT_NO" VARCHAR2(50), 
	"EMP_ID" VARCHAR2(50)
   ) ;
--------------------------------------------------------
--  DDL for Table SCHEDULE
--------------------------------------------------------

  CREATE TABLE "FUADRAFID"."SCHEDULE" 
   (	"SCHEDULE_NO" VARCHAR2(50), 
	"DAY" VARCHAR2(50), 
	"ARRIVAL_TIME" VARCHAR2(50), 
	"DEPARTURE_TIME" VARCHAR2(50), 
	"DIVISION" VARCHAR2(50), 
	"DISTRICT" VARCHAR2(50), 
	"POLICE_STATION" VARCHAR2(50), 
	"STREET" VARCHAR2(50)
   ) ;
--------------------------------------------------------
--  DDL for Table SCHEDULING
--------------------------------------------------------

  CREATE TABLE "FUADRAFID"."SCHEDULING" 
   (	"REG_NO" VARCHAR2(50), 
	"SCHEDULE_NO" VARCHAR2(50)
   ) ;
--------------------------------------------------------
--  DDL for Table SUPPLIED_BY
--------------------------------------------------------

  CREATE TABLE "FUADRAFID"."SUPPLIED_BY" 
   (	"ISBN_NO" VARCHAR2(50), 
	"STORE_ID" VARCHAR2(50)
   ) ;
--------------------------------------------------------
--  DDL for Table VEHICLE
--------------------------------------------------------

  CREATE TABLE "FUADRAFID"."VEHICLE" 
   (	"REG_NO" VARCHAR2(50), 
	"SERVICE_START_DATE" DATE, 
	"MODEL" VARCHAR2(50)
   ) ;
--------------------------------------------------------
--  DDL for Table VEHICLE_BOUGHT
--------------------------------------------------------

  CREATE TABLE "FUADRAFID"."VEHICLE_BOUGHT" 
   (	"RECEIPT_NO" VARCHAR2(50), 
	"REG_NO" VARCHAR2(50)
   ) ;
--------------------------------------------------------
--  DDL for Table VEHICLE_COMPONENTS
--------------------------------------------------------

  CREATE TABLE "FUADRAFID"."VEHICLE_COMPONENTS" 
   (	"REG_NO" VARCHAR2(50), 
	"SERIAL_NO" VARCHAR2(50)
   ) ;
--------------------------------------------------------
--  DDL for Table VOLUNTEERS
--------------------------------------------------------

  CREATE TABLE "FUADRAFID"."VOLUNTEERS" 
   (	"VOLUNTEER_ID" VARCHAR2(50), 
	"CONTACT_NO" VARCHAR2(50), 
	"CURRENT_DUTY" VARCHAR2(50), 
	"AGE" NUMBER(20,0)
   ) ;
--------------------------------------------------------
--  DDL for Table WRITTEN_BY
--------------------------------------------------------

  CREATE TABLE "FUADRAFID"."WRITTEN_BY" 
   (	"ISBN_NO" VARCHAR2(50), 
	"AUTHOR_ID" VARCHAR2(50)
   ) ;
--------------------------------------------------------
--  DDL for Sequence AUTHOR_ID_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "FUADRAFID"."AUTHOR_ID_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 53 CACHE 10 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence BOOKSTORE_ID_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "FUADRAFID"."BOOKSTORE_ID_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 33 CACHE 10 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence BOOK_ISBN
--------------------------------------------------------

   CREATE SEQUENCE  "FUADRAFID"."BOOK_ISBN"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 84 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence DEMO_CUST_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "FUADRAFID"."DEMO_CUST_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 21 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence DEMO_ORDER_ITEMS_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "FUADRAFID"."DEMO_ORDER_ITEMS_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 61 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence DEMO_ORD_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "FUADRAFID"."DEMO_ORD_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 11 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence DEMO_PROD_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "FUADRAFID"."DEMO_PROD_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 21 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence DEMO_USERS_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "FUADRAFID"."DEMO_USERS_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 21 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence EXP_SER
--------------------------------------------------------

   CREATE SEQUENCE  "FUADRAFID"."EXP_SER"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 55 CACHE 10 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence ISSUE_NUM
--------------------------------------------------------

   CREATE SEQUENCE  "FUADRAFID"."ISSUE_NUM"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 132 CACHE 10 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence MAINT_NO
--------------------------------------------------------

   CREATE SEQUENCE  "FUADRAFID"."MAINT_NO"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 30 CACHE 10 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence MEM_ID
--------------------------------------------------------

   CREATE SEQUENCE  "FUADRAFID"."MEM_ID"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 104 CACHE 10 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence PUB_REG_NO_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "FUADRAFID"."PUB_REG_NO_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 33 CACHE 10 NOORDER  NOCYCLE ;
REM INSERTING into FUADRAFID.ARRANGE
SET DEFINE OFF;
REM INSERTING into FUADRAFID.AUTHOR
SET DEFINE OFF;
REM INSERTING into FUADRAFID.BOOKS
SET DEFINE OFF;
REM INSERTING into FUADRAFID.BOOKS_BOUGHT
SET DEFINE OFF;
REM INSERTING into FUADRAFID.BOOK_ON_VEHICLE
SET DEFINE OFF;
REM INSERTING into FUADRAFID.BOOK_SERIAL
SET DEFINE OFF;
REM INSERTING into FUADRAFID.BOOK_STORE
SET DEFINE OFF;
REM INSERTING into FUADRAFID.COMPONENTS
SET DEFINE OFF;
REM INSERTING into FUADRAFID.COMPONENTS_BOUGHT
SET DEFINE OFF;
REM INSERTING into FUADRAFID.CULTURAL_PROGRAM
SET DEFINE OFF;
REM INSERTING into FUADRAFID.DONATION
SET DEFINE OFF;
REM INSERTING into FUADRAFID.DONATORS
SET DEFINE OFF;
REM INSERTING into FUADRAFID.DRIVER
SET DEFINE OFF;
REM INSERTING into FUADRAFID.DRIVER_APPLICANTS
SET DEFINE OFF;
REM INSERTING into FUADRAFID.EDUCATIONAL_QUALIFICATION
SET DEFINE OFF;
REM INSERTING into FUADRAFID.EDU_QUAL_APP
SET DEFINE OFF;
REM INSERTING into FUADRAFID.EMPLOYEE
SET DEFINE OFF;
Insert into FUADRAFID.EMPLOYEE (EMP_ID,NAME_BANGLA,NAME_ENGLISH,ADDRESS,DATE_OF_BIRTH,CONTACT_NO,EMAIL,MARITAL_STATUS,DATE_OF_JOINING,SALARY,GENDER) values ('123','?????','rafid','sda',to_date('14-FEB-18','DD-MON-RR'),'01718955314','fuadrafid3@gmail.com','Unmarried',to_date('28-FEB-18','DD-MON-RR'),10000,'Male');
REM INSERTING into FUADRAFID.EXPENSE
SET DEFINE OFF;
REM INSERTING into FUADRAFID.HAD_ISSUE
SET DEFINE OFF;
REM INSERTING into FUADRAFID.HANDLES
SET DEFINE OFF;
Insert into FUADRAFID.HANDLES (EMP_ID,REG_NO,HANDLE_DATE) values ('123','ABC12G',to_date('01-FEB-18','DD-MON-RR'));
REM INSERTING into FUADRAFID.HANDLES_APPLICATION
SET DEFINE OFF;
REM INSERTING into FUADRAFID.HAS_ISSUE
SET DEFINE OFF;
REM INSERTING into FUADRAFID.HAS_VOLUNTEERS
SET DEFINE OFF;
REM INSERTING into FUADRAFID.HTMLDB_PLAN_TABLE
SET DEFINE OFF;
REM INSERTING into FUADRAFID.INCOME
SET DEFINE OFF;
Insert into FUADRAFID.INCOME (SERIAL_NO,AMOUNT,TRANSAC_DATE) values ('A-045',100,to_date('23-FEB-18','DD-MON-RR'));
REM INSERTING into FUADRAFID.ISSUE
SET DEFINE OFF;
REM INSERTING into FUADRAFID.ISSUED_TO
SET DEFINE OFF;
REM INSERTING into FUADRAFID.JOB_APPLICANTS
SET DEFINE OFF;
REM INSERTING into FUADRAFID.LIBRARIAN
SET DEFINE OFF;
Insert into FUADRAFID.LIBRARIAN (EMP_ID,WORKING_PLACE,SPECIALIZATION) values ('123','Dhaka','Books');
REM INSERTING into FUADRAFID.LIBRARIAN_APPLICANTS
SET DEFINE OFF;
REM INSERTING into FUADRAFID.LOGIN_LIBRARIANS
SET DEFINE OFF;
Insert into FUADRAFID.LOGIN_LIBRARIANS (USERNAME,EMP_ID,PASSWORD) values ('fuadrafid','123','muhtasim');
REM INSERTING into FUADRAFID.LOGIN_MEMBERS
SET DEFINE OFF;
Insert into FUADRAFID.LOGIN_MEMBERS (USERNAME,MEMBER_ID,PASSWORD) values ('muhtasim','M-094','fuadrafid');
REM INSERTING into FUADRAFID.LOGIN_OFFICERS
SET DEFINE OFF;
REM INSERTING into FUADRAFID.MAINTAINANCE
SET DEFINE OFF;
REM INSERTING into FUADRAFID.MAINTAINANCE_COST
SET DEFINE OFF;
REM INSERTING into FUADRAFID.MEMBERS
SET DEFINE OFF;
Insert into FUADRAFID.MEMBERS (MEMBER_ID,MEMBER_NAME,CONTACT_NO,EMAIL_ADDRESS,GENDER,JOINING_DATE,MEMBERSHIP_TYPE) values ('M-094','Muhtasim Fuad Rafid','01718955314','fuadrafid@gmail.com','Male',to_date('23-FEB-18','DD-MON-RR'),'General');
REM INSERTING into FUADRAFID.MEMBER_DEPOSITIONS
SET DEFINE OFF;
Insert into FUADRAFID.MEMBER_DEPOSITIONS (MEMBER_ID,SERIAL_NO) values ('M-094','A-045');
REM INSERTING into FUADRAFID.MEMBER_ON_VEHICLE
SET DEFINE OFF;
Insert into FUADRAFID.MEMBER_ON_VEHICLE (MEMBER_ID,REG_NO) values ('M-094','ABC12G');
REM INSERTING into FUADRAFID.OFFICER
SET DEFINE OFF;
REM INSERTING into FUADRAFID.OFFICER_APPLICANTS
SET DEFINE OFF;
REM INSERTING into FUADRAFID.PRIZE_WINNERS
SET DEFINE OFF;
REM INSERTING into FUADRAFID.PROGRAM_EXPENSE
SET DEFINE OFF;
REM INSERTING into FUADRAFID.PUBLISHED_BY
SET DEFINE OFF;
REM INSERTING into FUADRAFID.PUBLISHER
SET DEFINE OFF;
REM INSERTING into FUADRAFID.REQUESTS_FOR
SET DEFINE OFF;
REM INSERTING into FUADRAFID.SALARY_GIVEN_TO
SET DEFINE OFF;
REM INSERTING into FUADRAFID.SCHEDULE
SET DEFINE OFF;
REM INSERTING into FUADRAFID.SCHEDULING
SET DEFINE OFF;
REM INSERTING into FUADRAFID.SUPPLIED_BY
SET DEFINE OFF;
REM INSERTING into FUADRAFID.VEHICLE
SET DEFINE OFF;
Insert into FUADRAFID.VEHICLE (REG_NO,SERVICE_START_DATE,MODEL) values ('ABC12G',to_date('04-FEB-18','DD-MON-RR'),'MERCEDES BENZ');
REM INSERTING into FUADRAFID.VEHICLE_BOUGHT
SET DEFINE OFF;
REM INSERTING into FUADRAFID.VEHICLE_COMPONENTS
SET DEFINE OFF;
REM INSERTING into FUADRAFID.VOLUNTEERS
SET DEFINE OFF;
REM INSERTING into FUADRAFID.WRITTEN_BY
SET DEFINE OFF;
--------------------------------------------------------
--  DDL for Index PK_EMP_ID_DRIVER
--------------------------------------------------------

  CREATE UNIQUE INDEX "FUADRAFID"."PK_EMP_ID_DRIVER" ON "FUADRAFID"."DRIVER" ("EMP_ID") 
  ;
--------------------------------------------------------
--  DDL for Index IDX_MEM_NAME
--------------------------------------------------------

  CREATE INDEX "FUADRAFID"."IDX_MEM_NAME" ON "FUADRAFID"."MEMBERS" ("MEMBER_NAME") 
  ;
--------------------------------------------------------
--  DDL for Index BOOKS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "FUADRAFID"."BOOKS_PK" ON "FUADRAFID"."BOOKS" ("ISBN_NO") 
  ;
--------------------------------------------------------
--  DDL for Index PK_PROG_NO
--------------------------------------------------------

  CREATE UNIQUE INDEX "FUADRAFID"."PK_PROG_NO" ON "FUADRAFID"."CULTURAL_PROGRAM" ("PROG_NO") 
  ;
--------------------------------------------------------
--  DDL for Index HAS_ISSUE_UK1
--------------------------------------------------------

  CREATE UNIQUE INDEX "FUADRAFID"."HAS_ISSUE_UK1" ON "FUADRAFID"."HAS_ISSUE" ("ISBN_NO", "SERIAL_NO") 
  ;
--------------------------------------------------------
--  DDL for Index PK_STORE_ID
--------------------------------------------------------

  CREATE UNIQUE INDEX "FUADRAFID"."PK_STORE_ID" ON "FUADRAFID"."BOOK_STORE" ("STORE_ID") 
  ;
--------------------------------------------------------
--  DDL for Index PK_VOLUNTEER_ID
--------------------------------------------------------

  CREATE UNIQUE INDEX "FUADRAFID"."PK_VOLUNTEER_ID" ON "FUADRAFID"."VOLUNTEERS" ("VOLUNTEER_ID") 
  ;
--------------------------------------------------------
--  DDL for Index PK_APP_NO_JOB
--------------------------------------------------------

  CREATE UNIQUE INDEX "FUADRAFID"."PK_APP_NO_JOB" ON "FUADRAFID"."JOB_APPLICANTS" ("APPLICATION_NO") 
  ;
--------------------------------------------------------
--  DDL for Index PK_AUTHOR_ID
--------------------------------------------------------

  CREATE UNIQUE INDEX "FUADRAFID"."PK_AUTHOR_ID" ON "FUADRAFID"."AUTHOR" ("AUTHOR_ID") 
  ;
--------------------------------------------------------
--  DDL for Index PK_DON_ID
--------------------------------------------------------

  CREATE UNIQUE INDEX "FUADRAFID"."PK_DON_ID" ON "FUADRAFID"."DONATORS" ("DONATOR_ID") 
  ;
--------------------------------------------------------
--  DDL for Index PK_EMP_ID_LIB
--------------------------------------------------------

  CREATE UNIQUE INDEX "FUADRAFID"."PK_EMP_ID_LIB" ON "FUADRAFID"."LIBRARIAN" ("EMP_ID") 
  ;
--------------------------------------------------------
--  DDL for Index PK_MEMBER_ID
--------------------------------------------------------

  CREATE UNIQUE INDEX "FUADRAFID"."PK_MEMBER_ID" ON "FUADRAFID"."MEMBERS" ("MEMBER_ID") 
  ;
--------------------------------------------------------
--  DDL for Index PK_RECEIPT_NO
--------------------------------------------------------

  CREATE UNIQUE INDEX "FUADRAFID"."PK_RECEIPT_NO" ON "FUADRAFID"."EXPENSE" ("RECEIPT_NO") 
  ;
--------------------------------------------------------
--  DDL for Index PK_SERIAL_NO
--------------------------------------------------------

  CREATE UNIQUE INDEX "FUADRAFID"."PK_SERIAL_NO" ON "FUADRAFID"."INCOME" ("SERIAL_NO") 
  ;
--------------------------------------------------------
--  DDL for Index PK_SCHEDULE
--------------------------------------------------------

  CREATE UNIQUE INDEX "FUADRAFID"."PK_SCHEDULE" ON "FUADRAFID"."SCHEDULE" ("SCHEDULE_NO") 
  ;
--------------------------------------------------------
--  DDL for Index PK_P_REG_NO
--------------------------------------------------------

  CREATE UNIQUE INDEX "FUADRAFID"."PK_P_REG_NO" ON "FUADRAFID"."PUBLISHER" ("P_REG_NO") 
  ;
--------------------------------------------------------
--  DDL for Index PK_SERIAL_NO_COMP
--------------------------------------------------------

  CREATE UNIQUE INDEX "FUADRAFID"."PK_SERIAL_NO_COMP" ON "FUADRAFID"."COMPONENTS" ("SERIAL_NO") 
  ;
--------------------------------------------------------
--  DDL for Index ISSUE_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "FUADRAFID"."ISSUE_PK" ON "FUADRAFID"."ISSUE" ("ISSUE_NO") 
  ;
--------------------------------------------------------
--  DDL for Index PK_REG_NO
--------------------------------------------------------

  CREATE UNIQUE INDEX "FUADRAFID"."PK_REG_NO" ON "FUADRAFID"."VEHICLE" ("REG_NO") 
  ;
--------------------------------------------------------
--  DDL for Index LIBRARIAN_APPLICANTS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "FUADRAFID"."LIBRARIAN_APPLICANTS_PK" ON "FUADRAFID"."LIBRARIAN_APPLICANTS" ("APPLICATION_NO") 
  ;
--------------------------------------------------------
--  DDL for Index PK_EMP_ID_OFF
--------------------------------------------------------

  CREATE UNIQUE INDEX "FUADRAFID"."PK_EMP_ID_OFF" ON "FUADRAFID"."OFFICER" ("EMP_ID") 
  ;
--------------------------------------------------------
--  DDL for Index BOOK_SERIAL_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "FUADRAFID"."BOOK_SERIAL_PK" ON "FUADRAFID"."BOOK_SERIAL" ("ISBN_NO", "SERIAL_NO") 
  ;
--------------------------------------------------------
--  DDL for Index PK_SERIAL_NO_MAIN
--------------------------------------------------------

  CREATE UNIQUE INDEX "FUADRAFID"."PK_SERIAL_NO_MAIN" ON "FUADRAFID"."MAINTAINANCE" ("SERIAL_NO", "REG_NO") 
  ;
--------------------------------------------------------
--  DDL for Index IDX_BOOK_NAME
--------------------------------------------------------

  CREATE INDEX "FUADRAFID"."IDX_BOOK_NAME" ON "FUADRAFID"."BOOKS" ("BOOKS_NAME") 
  ;
--------------------------------------------------------
--  DDL for Index OFFICER_APPLICANTS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "FUADRAFID"."OFFICER_APPLICANTS_PK" ON "FUADRAFID"."OFFICER_APPLICANTS" ("APPLICATION_NO") 
  ;
--------------------------------------------------------
--  DDL for Index PK_EMP_ID
--------------------------------------------------------

  CREATE UNIQUE INDEX "FUADRAFID"."PK_EMP_ID" ON "FUADRAFID"."EMPLOYEE" ("EMP_ID") 
  ;
--------------------------------------------------------
--  DDL for Trigger AUTO_ENTRY_EXP_DATE_ISSUE
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "FUADRAFID"."AUTO_ENTRY_EXP_DATE_ISSUE" 
before INSERT ON ISSUE
FOR EACH ROW
declare
   nmbr date;
BEGIN 
    Select sysdate into nmbr from dual;
    :new.issue_date:=nmbr;
END;

/
ALTER TRIGGER "FUADRAFID"."AUTO_ENTRY_EXP_DATE_ISSUE" ENABLE;
--------------------------------------------------------
--  DDL for Trigger AUTO_ENTRY_ISSUE_DATE_ISSUE
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "FUADRAFID"."AUTO_ENTRY_ISSUE_DATE_ISSUE" 
before INSERT ON ISSUE
FOR EACH ROW
declare
   nmbr date;
BEGIN 
    Select sysdate into nmbr from dual;
    :new.issue_date:=nmbr;
END;

/
ALTER TRIGGER "FUADRAFID"."AUTO_ENTRY_ISSUE_DATE_ISSUE" ENABLE;
--------------------------------------------------------
--  DDL for Trigger AUTO_ENTRY_REQ_DATE
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "FUADRAFID"."AUTO_ENTRY_REQ_DATE" 
before INSERT ON REQUESTS_FOR
FOR EACH ROW
declare
   nmbr date;
BEGIN 
    Select sysdate into nmbr from dual;
    :new.req_date:=nmbr;
END;

/
ALTER TRIGGER "FUADRAFID"."AUTO_ENTRY_REQ_DATE" ENABLE;
--------------------------------------------------------
--  DDL for Trigger AUTO_UPDATE_INCOME
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "FUADRAFID"."AUTO_UPDATE_INCOME" after insert on MEMBERS for each row 
declare 
var_temp varchar2(50);
var_date DATE;
begin
select concat('A-0',exp_ser.nextval) into var_temp from dual;
dbms_output.put_line(var_temp);
select sysdate into var_date from dual;
if :new.MEMBERSHIP_TYPE='Special'
then 
insert into income (SERIAL_NO,AMOUNT,TRANSAC_DATE) values (var_temp,200,var_date); 
elsif :new.MEMBERSHIP_TYPE='General'
then 
insert into income(SERIAL_NO,AMOUNT,TRANSAC_DATE) values (var_temp,100,var_date); 
end if;
insert into member_depositions(member_id,serial_no) values (:new.member_id,var_temp);
end;

/
ALTER TRIGGER "FUADRAFID"."AUTO_UPDATE_INCOME" ENABLE;
--------------------------------------------------------
--  DDL for Trigger MAINT_ENTRY
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "FUADRAFID"."MAINT_ENTRY" 
INSTEAD OF INSERT ON LIB_MAINT
BEGIN
INSERT INTO MAINTAINANCE values('Y-0'||maint_no.nextval,:new.REG_NO,:new.DESCRIPTION,:new.DATE_DONE,:new.NEXT_DUE_DATE);
INSERT INTO EXPENSE VALUES('V-0'||exp_ser.nextval,:new.amount,sysdate);
INSERT INTO MAINTAINANCE_COST VALUES( 'V-0'||exp_ser.currval,:new.REG_NO,'Y-0'||maint_no.currval);
end;

/
ALTER TRIGGER "FUADRAFID"."MAINT_ENTRY" ENABLE;
--------------------------------------------------------
--  DDL for Trigger MEM_INSERT
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "FUADRAFID"."MEM_INSERT" 
instead of insert ON lib_mem
declare 
memb_id varchar2(50);
regi_no varchar2(50);
begin
select 'M-0'||mem_id.nextval into memb_id from dual;
insert into members values(memb_id,:new.name,:new.contact_no,:new.email_address,:new.gender,sysdate,:new.membership_type);
select reg_no into regi_no from handles where emp_id=:new.lib_id;
insert into member_on_vehicle values(memb_id,regi_no);
insert into login_members values (:new.username,memb_id,:new.pass);
end;

/
ALTER TRIGGER "FUADRAFID"."MEM_INSERT" ENABLE;
--------------------------------------------------------
--  DDL for Trigger ON_ISSUE_DELETE_SERIAL
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "FUADRAFID"."ON_ISSUE_DELETE_SERIAL" 
before insert on HAS_ISSUE
for each row
begin
DELETE FROM book_serial WHERE isbn_no=:new.isbn_no and serial_no=:new.serial_no;
end;

/
ALTER TRIGGER "FUADRAFID"."ON_ISSUE_DELETE_SERIAL" ENABLE;
--------------------------------------------------------
--  DDL for Trigger ON_RETURN_ADD_SERIAL
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "FUADRAFID"."ON_RETURN_ADD_SERIAL" 
before delete on HAS_ISSUE
for each row
begin
insert into book_serial values(:old.isbn_no,:old.serial_no);
insert into had_issue values(:old.isbn_no,:old.issue_no);
update issue set return_date=sysdate where issue_no=:old.issue_no;
end;

/
ALTER TRIGGER "FUADRAFID"."ON_RETURN_ADD_SERIAL" ENABLE;
--------------------------------------------------------
--  DDL for Procedure ISSUE_BOOK
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "FUADRAFID"."ISSUE_BOOK" (mem_id VARCHAR2,book_id VARCHAR2,lib_id VARCHAR2,seri_no VARCHAR2 ) 
IS
serial_num varchar2(50);
issue_numb varchar2(50);
cursor ser_no is select  serial_no from book_serial natural join BOOK_ON_VEHICLE natural join Handles where ISBN_NO=book_id and serial_no=seri_no;
BEGIN
open ser_no;
serial_num:=null;
fetch ser_no into serial_num;
DBMS_OUTPUT.PUT_LINE(serial_num);
if serial_num is not null then
select issue_num.nextval into issue_numb from dual;
insert into issue (issue_no,issue_date)values (issue_numb,sysdate);
insert into issued_to values (issue_numb,mem_id);
insert into has_issue values(book_id,serial_num,issue_numb);
end if;
close ser_no;
end;

/
--------------------------------------------------------
--  DDL for Procedure REMOVE_MEM
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "FUADRAFID"."REMOVE_MEM" (mem_id varchar2 ) AS
   BEGIN
      DELETE FROM members
      WHERE member_id=mem_id;
   END;

/
--------------------------------------------------------
--  Constraints for Table SCHEDULING
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."SCHEDULING" MODIFY ("REG_NO" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table REQUESTS_FOR
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."REQUESTS_FOR" MODIFY ("MEMBER_ID" NOT NULL ENABLE);
  ALTER TABLE "FUADRAFID"."REQUESTS_FOR" MODIFY ("ISBN_NO" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table EMPLOYEE
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."EMPLOYEE" MODIFY ("EMP_ID" NOT NULL ENABLE);
  ALTER TABLE "FUADRAFID"."EMPLOYEE" ADD CONSTRAINT "PK_EMP_ID" PRIMARY KEY ("EMP_ID") ENABLE;
  ALTER TABLE "FUADRAFID"."EMPLOYEE" MODIFY ("NAME_BANGLA" NOT NULL ENABLE);
  ALTER TABLE "FUADRAFID"."EMPLOYEE" MODIFY ("NAME_ENGLISH" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table COMPONENTS_BOUGHT
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."COMPONENTS_BOUGHT" MODIFY ("RECEIPT_NO" NOT NULL ENABLE);
  ALTER TABLE "FUADRAFID"."COMPONENTS_BOUGHT" MODIFY ("SERIAL_NO" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SUPPLIED_BY
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."SUPPLIED_BY" MODIFY ("ISBN_NO" NOT NULL ENABLE);
  ALTER TABLE "FUADRAFID"."SUPPLIED_BY" MODIFY ("STORE_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table EDU_QUAL_APP
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."EDU_QUAL_APP" MODIFY ("APPLICATION_NO" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table MAINTAINANCE_COST
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."MAINTAINANCE_COST" MODIFY ("RECEIPT_NO" NOT NULL ENABLE);
  ALTER TABLE "FUADRAFID"."MAINTAINANCE_COST" MODIFY ("REG_NO" NOT NULL ENABLE);
  ALTER TABLE "FUADRAFID"."MAINTAINANCE_COST" MODIFY ("SERIAL_NO" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table HAS_ISSUE
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."HAS_ISSUE" MODIFY ("ISBN_NO" NOT NULL ENABLE);
  ALTER TABLE "FUADRAFID"."HAS_ISSUE" MODIFY ("SERIAL_NO" NOT NULL ENABLE);
  ALTER TABLE "FUADRAFID"."HAS_ISSUE" MODIFY ("ISSUE_NO" NOT NULL ENABLE);
  ALTER TABLE "FUADRAFID"."HAS_ISSUE" ADD CONSTRAINT "HAS_ISSUE_UK1" UNIQUE ("ISBN_NO", "SERIAL_NO") ENABLE;
--------------------------------------------------------
--  Constraints for Table ARRANGE
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."ARRANGE" MODIFY ("EMP_ID" NOT NULL ENABLE);
  ALTER TABLE "FUADRAFID"."ARRANGE" MODIFY ("PROG_NO" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table DONATION
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."DONATION" MODIFY ("SERIAL_NO" NOT NULL ENABLE);
  ALTER TABLE "FUADRAFID"."DONATION" MODIFY ("DONATOR_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table HAD_ISSUE
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."HAD_ISSUE" MODIFY ("ISBN_NO" NOT NULL ENABLE);
  ALTER TABLE "FUADRAFID"."HAD_ISSUE" MODIFY ("ISSUE_NO" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table MEMBERS
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."MEMBERS" MODIFY ("MEMBER_ID" NOT NULL ENABLE);
  ALTER TABLE "FUADRAFID"."MEMBERS" ADD CONSTRAINT "PK_MEMBER_ID" PRIMARY KEY ("MEMBER_ID") ENABLE;
--------------------------------------------------------
--  Constraints for Table VEHICLE_COMPONENTS
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."VEHICLE_COMPONENTS" MODIFY ("REG_NO" NOT NULL ENABLE);
  ALTER TABLE "FUADRAFID"."VEHICLE_COMPONENTS" MODIFY ("SERIAL_NO" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table BOOK_SERIAL
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."BOOK_SERIAL" ADD CONSTRAINT "BOOK_SERIAL_PK" PRIMARY KEY ("ISBN_NO", "SERIAL_NO") ENABLE;
--------------------------------------------------------
--  Constraints for Table OFFICER
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."OFFICER" MODIFY ("EMP_ID" NOT NULL ENABLE);
  ALTER TABLE "FUADRAFID"."OFFICER" ADD CONSTRAINT "PK_EMP_ID_OFF" PRIMARY KEY ("EMP_ID") ENABLE;
--------------------------------------------------------
--  Constraints for Table SCHEDULE
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."SCHEDULE" ADD CONSTRAINT "PK_SCHEDULE" PRIMARY KEY ("SCHEDULE_NO") ENABLE;
--------------------------------------------------------
--  Constraints for Table WRITTEN_BY
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."WRITTEN_BY" MODIFY ("ISBN_NO" NOT NULL ENABLE);
  ALTER TABLE "FUADRAFID"."WRITTEN_BY" MODIFY ("AUTHOR_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table VEHICLE
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."VEHICLE" MODIFY ("REG_NO" NOT NULL ENABLE);
  ALTER TABLE "FUADRAFID"."VEHICLE" ADD CONSTRAINT "PK_REG_NO" PRIMARY KEY ("REG_NO") ENABLE;
--------------------------------------------------------
--  Constraints for Table LOGIN_OFFICERS
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."LOGIN_OFFICERS" MODIFY ("USERNAME" NOT NULL ENABLE);
  ALTER TABLE "FUADRAFID"."LOGIN_OFFICERS" MODIFY ("EMP_ID" NOT NULL ENABLE);
  ALTER TABLE "FUADRAFID"."LOGIN_OFFICERS" MODIFY ("PASSWORD" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table LIBRARIAN_APPLICANTS
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."LIBRARIAN_APPLICANTS" MODIFY ("APPLICATION_NO" NOT NULL ENABLE);
  ALTER TABLE "FUADRAFID"."LIBRARIAN_APPLICANTS" ADD CONSTRAINT "LIBRARIAN_APPLICANTS_PK" PRIMARY KEY ("APPLICATION_NO") ENABLE;
--------------------------------------------------------
--  Constraints for Table COMPONENTS
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."COMPONENTS" MODIFY ("SERIAL_NO" NOT NULL ENABLE);
  ALTER TABLE "FUADRAFID"."COMPONENTS" ADD CONSTRAINT "PK_SERIAL_NO_COMP" PRIMARY KEY ("SERIAL_NO") ENABLE;
--------------------------------------------------------
--  Constraints for Table BOOKS_BOUGHT
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."BOOKS_BOUGHT" MODIFY ("ISBN_NO" NOT NULL ENABLE);
  ALTER TABLE "FUADRAFID"."BOOKS_BOUGHT" MODIFY ("RECEIPT_NO" NOT NULL ENABLE);
  ALTER TABLE "FUADRAFID"."BOOKS_BOUGHT" MODIFY ("QUANTITY" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table INCOME
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."INCOME" MODIFY ("SERIAL_NO" NOT NULL ENABLE);
  ALTER TABLE "FUADRAFID"."INCOME" ADD CONSTRAINT "PK_SERIAL_NO" PRIMARY KEY ("SERIAL_NO") ENABLE;
  ALTER TABLE "FUADRAFID"."INCOME" MODIFY ("AMOUNT" NOT NULL ENABLE);
  ALTER TABLE "FUADRAFID"."INCOME" ADD CONSTRAINT "INCOME_CHK1" CHECK (AMOUNT>0) ENABLE;
--------------------------------------------------------
--  Constraints for Table BOOK_STORE
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."BOOK_STORE" MODIFY ("STORE_ID" NOT NULL ENABLE);
  ALTER TABLE "FUADRAFID"."BOOK_STORE" ADD CONSTRAINT "PK_STORE_ID" PRIMARY KEY ("STORE_ID") ENABLE;
--------------------------------------------------------
--  Constraints for Table OFFICER_APPLICANTS
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."OFFICER_APPLICANTS" MODIFY ("APPLICATION_NO" NOT NULL ENABLE);
  ALTER TABLE "FUADRAFID"."OFFICER_APPLICANTS" ADD CONSTRAINT "OFFICER_APPLICANTS_PK" PRIMARY KEY ("APPLICATION_NO") ENABLE;
--------------------------------------------------------
--  Constraints for Table HAS_VOLUNTEERS
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."HAS_VOLUNTEERS" MODIFY ("VOLUNTEER_ID" NOT NULL ENABLE);
  ALTER TABLE "FUADRAFID"."HAS_VOLUNTEERS" MODIFY ("PROG_NO" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table EXPENSE
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."EXPENSE" MODIFY ("RECEIPT_NO" NOT NULL ENABLE);
  ALTER TABLE "FUADRAFID"."EXPENSE" ADD CONSTRAINT "PK_RECEIPT_NO" PRIMARY KEY ("RECEIPT_NO") ENABLE;
  ALTER TABLE "FUADRAFID"."EXPENSE" ADD CONSTRAINT "EXPENSE_CHK1" CHECK ((AMOUNT>0)) ENABLE;
--------------------------------------------------------
--  Constraints for Table DRIVER
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."DRIVER" MODIFY ("EMP_ID" NOT NULL ENABLE);
  ALTER TABLE "FUADRAFID"."DRIVER" ADD CONSTRAINT "PK_EMP_ID_DRIVER" PRIMARY KEY ("EMP_ID") ENABLE;
--------------------------------------------------------
--  Constraints for Table ISSUE
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."ISSUE" MODIFY ("ISSUE_NO" NOT NULL ENABLE);
  ALTER TABLE "FUADRAFID"."ISSUE" ADD CONSTRAINT "ISSUE_PK" PRIMARY KEY ("ISSUE_NO") ENABLE;
--------------------------------------------------------
--  Constraints for Table PUBLISHED_BY
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."PUBLISHED_BY" MODIFY ("ISBN_NO" NOT NULL ENABLE);
  ALTER TABLE "FUADRAFID"."PUBLISHED_BY" MODIFY ("P_REG_NO" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SALARY_GIVEN_TO
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."SALARY_GIVEN_TO" MODIFY ("RECEIPT_NO" NOT NULL ENABLE);
  ALTER TABLE "FUADRAFID"."SALARY_GIVEN_TO" MODIFY ("EMP_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table VEHICLE_BOUGHT
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."VEHICLE_BOUGHT" MODIFY ("RECEIPT_NO" NOT NULL ENABLE);
  ALTER TABLE "FUADRAFID"."VEHICLE_BOUGHT" MODIFY ("REG_NO" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table VOLUNTEERS
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."VOLUNTEERS" MODIFY ("VOLUNTEER_ID" NOT NULL ENABLE);
  ALTER TABLE "FUADRAFID"."VOLUNTEERS" ADD CONSTRAINT "PK_VOLUNTEER_ID" PRIMARY KEY ("VOLUNTEER_ID") ENABLE;
--------------------------------------------------------
--  Constraints for Table DRIVER_APPLICANTS
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."DRIVER_APPLICANTS" MODIFY ("APPLICATION_NO" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table MAINTAINANCE
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."MAINTAINANCE" MODIFY ("SERIAL_NO" NOT NULL ENABLE);
  ALTER TABLE "FUADRAFID"."MAINTAINANCE" MODIFY ("REG_NO" NOT NULL ENABLE);
  ALTER TABLE "FUADRAFID"."MAINTAINANCE" ADD CONSTRAINT "PK_SERIAL_NO_MAIN" PRIMARY KEY ("SERIAL_NO", "REG_NO") ENABLE;
--------------------------------------------------------
--  Constraints for Table PRIZE_WINNERS
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."PRIZE_WINNERS" MODIFY ("PROG_NO" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table PROGRAM_EXPENSE
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."PROGRAM_EXPENSE" MODIFY ("RECEIPT_NO" NOT NULL ENABLE);
  ALTER TABLE "FUADRAFID"."PROGRAM_EXPENSE" MODIFY ("PROG_NO" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table DONATORS
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."DONATORS" MODIFY ("DONATOR_ID" NOT NULL ENABLE);
  ALTER TABLE "FUADRAFID"."DONATORS" ADD CONSTRAINT "PK_DON_ID" PRIMARY KEY ("DONATOR_ID") ENABLE;
--------------------------------------------------------
--  Constraints for Table HANDLES
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."HANDLES" MODIFY ("EMP_ID" NOT NULL ENABLE);
  ALTER TABLE "FUADRAFID"."HANDLES" MODIFY ("REG_NO" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table PUBLISHER
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."PUBLISHER" MODIFY ("NAME" NOT NULL ENABLE);
  ALTER TABLE "FUADRAFID"."PUBLISHER" MODIFY ("P_REG_NO" NOT NULL ENABLE);
  ALTER TABLE "FUADRAFID"."PUBLISHER" ADD CONSTRAINT "PK_P_REG_NO" PRIMARY KEY ("P_REG_NO") ENABLE;
--------------------------------------------------------
--  Constraints for Table JOB_APPLICANTS
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."JOB_APPLICANTS" MODIFY ("APPLICATION_NO" NOT NULL ENABLE);
  ALTER TABLE "FUADRAFID"."JOB_APPLICANTS" ADD CONSTRAINT "PK_APP_NO_JOB" PRIMARY KEY ("APPLICATION_NO") ENABLE;
  ALTER TABLE "FUADRAFID"."JOB_APPLICANTS" MODIFY ("NAME" NOT NULL ENABLE);
  ALTER TABLE "FUADRAFID"."JOB_APPLICANTS" MODIFY ("CONTACT_NO" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table EDUCATIONAL_QUALIFICATION
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."EDUCATIONAL_QUALIFICATION" MODIFY ("EMP_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table BOOKS
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."BOOKS" MODIFY ("ISBN_NO" NOT NULL ENABLE);
  ALTER TABLE "FUADRAFID"."BOOKS" ADD CONSTRAINT "BOOKS_PK" PRIMARY KEY ("ISBN_NO") ENABLE;
  ALTER TABLE "FUADRAFID"."BOOKS" MODIFY ("BOOKS_NAME" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table LIBRARIAN
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."LIBRARIAN" MODIFY ("EMP_ID" NOT NULL ENABLE);
  ALTER TABLE "FUADRAFID"."LIBRARIAN" ADD CONSTRAINT "PK_EMP_ID_LIB" PRIMARY KEY ("EMP_ID") ENABLE;
--------------------------------------------------------
--  Constraints for Table CULTURAL_PROGRAM
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."CULTURAL_PROGRAM" MODIFY ("PROG_NO" NOT NULL ENABLE);
  ALTER TABLE "FUADRAFID"."CULTURAL_PROGRAM" ADD CONSTRAINT "PK_PROG_NO" PRIMARY KEY ("PROG_NO") ENABLE;
--------------------------------------------------------
--  Constraints for Table AUTHOR
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."AUTHOR" MODIFY ("AUTHOR_ID" NOT NULL ENABLE);
  ALTER TABLE "FUADRAFID"."AUTHOR" ADD CONSTRAINT "PK_AUTHOR_ID" PRIMARY KEY ("AUTHOR_ID") ENABLE;
  ALTER TABLE "FUADRAFID"."AUTHOR" MODIFY ("NAME" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table MEMBER_DEPOSITIONS
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."MEMBER_DEPOSITIONS" MODIFY ("MEMBER_ID" NOT NULL ENABLE);
  ALTER TABLE "FUADRAFID"."MEMBER_DEPOSITIONS" MODIFY ("SERIAL_NO" NOT NULL ENABLE);
--------------------------------------------------------
--  Ref Constraints for Table ARRANGE
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."ARRANGE" ADD CONSTRAINT "FK_ARRANGE_EMP_ID" FOREIGN KEY ("EMP_ID")
	  REFERENCES "FUADRAFID"."EMPLOYEE" ("EMP_ID") ON DELETE CASCADE ENABLE;
  ALTER TABLE "FUADRAFID"."ARRANGE" ADD CONSTRAINT "FK_ARRANGE_PROG_NO" FOREIGN KEY ("PROG_NO")
	  REFERENCES "FUADRAFID"."CULTURAL_PROGRAM" ("PROG_NO") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table BOOKS_BOUGHT
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."BOOKS_BOUGHT" ADD CONSTRAINT "BOOKS_BOUGHT_FK1" FOREIGN KEY ("ISBN_NO")
	  REFERENCES "FUADRAFID"."BOOKS" ("ISBN_NO") ENABLE;
  ALTER TABLE "FUADRAFID"."BOOKS_BOUGHT" ADD CONSTRAINT "FK_RECEIPT_NO_BB" FOREIGN KEY ("RECEIPT_NO")
	  REFERENCES "FUADRAFID"."EXPENSE" ("RECEIPT_NO") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table BOOK_ON_VEHICLE
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."BOOK_ON_VEHICLE" ADD CONSTRAINT "BOOK_ON_VEHICLE_FK1" FOREIGN KEY ("ISBN_NO", "SERIAL_NO")
	  REFERENCES "FUADRAFID"."BOOK_SERIAL" ("ISBN_NO", "SERIAL_NO") ENABLE;
  ALTER TABLE "FUADRAFID"."BOOK_ON_VEHICLE" ADD CONSTRAINT "BOOK_ON_VEHICLE_FK2" FOREIGN KEY ("REG_NO")
	  REFERENCES "FUADRAFID"."VEHICLE" ("REG_NO") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table BOOK_SERIAL
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."BOOK_SERIAL" ADD CONSTRAINT "BOOK_SERIAL_ISBN" FOREIGN KEY ("ISBN_NO")
	  REFERENCES "FUADRAFID"."BOOKS" ("ISBN_NO") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table COMPONENTS_BOUGHT
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."COMPONENTS_BOUGHT" ADD CONSTRAINT "FK_RECEIPT_NO_COMP" FOREIGN KEY ("RECEIPT_NO")
	  REFERENCES "FUADRAFID"."EXPENSE" ("RECEIPT_NO") ON DELETE CASCADE ENABLE;
  ALTER TABLE "FUADRAFID"."COMPONENTS_BOUGHT" ADD CONSTRAINT "FK_SERIAL_NO_COMP" FOREIGN KEY ("SERIAL_NO")
	  REFERENCES "FUADRAFID"."COMPONENTS" ("SERIAL_NO") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table DONATION
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."DONATION" ADD CONSTRAINT "FK_DON_ID_DONATORS" FOREIGN KEY ("DONATOR_ID")
	  REFERENCES "FUADRAFID"."DONATORS" ("DONATOR_ID") ENABLE;
  ALTER TABLE "FUADRAFID"."DONATION" ADD CONSTRAINT "FK_SERIAL_NO_INCOME" FOREIGN KEY ("SERIAL_NO")
	  REFERENCES "FUADRAFID"."INCOME" ("SERIAL_NO") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table DRIVER
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."DRIVER" ADD CONSTRAINT "FK_EMP_ID_DRIVER" FOREIGN KEY ("EMP_ID")
	  REFERENCES "FUADRAFID"."EMPLOYEE" ("EMP_ID") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table DRIVER_APPLICANTS
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."DRIVER_APPLICANTS" ADD CONSTRAINT "FK_APP_NO_DR" FOREIGN KEY ("APPLICATION_NO")
	  REFERENCES "FUADRAFID"."JOB_APPLICANTS" ("APPLICATION_NO") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table EDUCATIONAL_QUALIFICATION
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."EDUCATIONAL_QUALIFICATION" ADD CONSTRAINT "FK_EMP_ID_EDU" FOREIGN KEY ("EMP_ID")
	  REFERENCES "FUADRAFID"."EMPLOYEE" ("EMP_ID") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table EDU_QUAL_APP
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."EDU_QUAL_APP" ADD CONSTRAINT "FK_APP_NO_EDU" FOREIGN KEY ("APPLICATION_NO")
	  REFERENCES "FUADRAFID"."JOB_APPLICANTS" ("APPLICATION_NO") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table HAD_ISSUE
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."HAD_ISSUE" ADD CONSTRAINT "HAD_ISSUE_FK1" FOREIGN KEY ("ISBN_NO", "ISSUE_NO")
	  REFERENCES "FUADRAFID"."BOOK_SERIAL" ("ISBN_NO", "SERIAL_NO") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table HANDLES
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."HANDLES" ADD CONSTRAINT "FK_EMP_ID_HAPP" FOREIGN KEY ("EMP_ID")
	  REFERENCES "FUADRAFID"."EMPLOYEE" ("EMP_ID") ON DELETE CASCADE ENABLE;
  ALTER TABLE "FUADRAFID"."HANDLES" ADD CONSTRAINT "FK_REG_NO_HAN" FOREIGN KEY ("REG_NO")
	  REFERENCES "FUADRAFID"."VEHICLE" ("REG_NO") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table HANDLES_APPLICATION
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."HANDLES_APPLICATION" ADD CONSTRAINT "FK_EMP_ID_HAN_APP" FOREIGN KEY ("EMP_ID")
	  REFERENCES "FUADRAFID"."EMPLOYEE" ("EMP_ID") ON DELETE CASCADE ENABLE;
  ALTER TABLE "FUADRAFID"."HANDLES_APPLICATION" ADD CONSTRAINT "HANDLES_APPLICATION_FK1" FOREIGN KEY ("APPLICATION_NO")
	  REFERENCES "FUADRAFID"."JOB_APPLICANTS" ("APPLICATION_NO") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table HAS_ISSUE
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."HAS_ISSUE" ADD CONSTRAINT "HAS_ISSUE_FK1" FOREIGN KEY ("ISBN_NO", "SERIAL_NO")
	  REFERENCES "FUADRAFID"."BOOK_SERIAL" ("ISBN_NO", "SERIAL_NO") ENABLE;
  ALTER TABLE "FUADRAFID"."HAS_ISSUE" ADD CONSTRAINT "HAS_ISSUE_FK2" FOREIGN KEY ("ISSUE_NO")
	  REFERENCES "FUADRAFID"."ISSUE" ("ISSUE_NO") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table HAS_VOLUNTEERS
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."HAS_VOLUNTEERS" ADD CONSTRAINT "FK_VOLUNTEER_ID" FOREIGN KEY ("VOLUNTEER_ID")
	  REFERENCES "FUADRAFID"."VOLUNTEERS" ("VOLUNTEER_ID") ON DELETE CASCADE ENABLE;
  ALTER TABLE "FUADRAFID"."HAS_VOLUNTEERS" ADD CONSTRAINT "FK_VOLUNTEER_PROG_NO" FOREIGN KEY ("PROG_NO")
	  REFERENCES "FUADRAFID"."CULTURAL_PROGRAM" ("PROG_NO") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table ISSUED_TO
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."ISSUED_TO" ADD CONSTRAINT "ISSUED_TO_FK1" FOREIGN KEY ("ISSUE_NO")
	  REFERENCES "FUADRAFID"."ISSUE" ("ISSUE_NO") ON DELETE CASCADE ENABLE;
  ALTER TABLE "FUADRAFID"."ISSUED_TO" ADD CONSTRAINT "ISSUED_TO_FK2" FOREIGN KEY ("MEMBER_ID")
	  REFERENCES "FUADRAFID"."MEMBERS" ("MEMBER_ID") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table LIBRARIAN
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."LIBRARIAN" ADD CONSTRAINT "FK_EMP_ID_LIB" FOREIGN KEY ("EMP_ID")
	  REFERENCES "FUADRAFID"."EMPLOYEE" ("EMP_ID") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table LIBRARIAN_APPLICANTS
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."LIBRARIAN_APPLICANTS" ADD CONSTRAINT "FK_APP_NO_LIB" FOREIGN KEY ("APPLICATION_NO")
	  REFERENCES "FUADRAFID"."JOB_APPLICANTS" ("APPLICATION_NO") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table LOGIN_LIBRARIANS
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."LOGIN_LIBRARIANS" ADD CONSTRAINT "FK_LOGIN_LIBRARIANS" FOREIGN KEY ("EMP_ID")
	  REFERENCES "FUADRAFID"."LIBRARIAN" ("EMP_ID") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table LOGIN_MEMBERS
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."LOGIN_MEMBERS" ADD CONSTRAINT "FK_LOGIN_MEMBERS" FOREIGN KEY ("MEMBER_ID")
	  REFERENCES "FUADRAFID"."MEMBERS" ("MEMBER_ID") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table LOGIN_OFFICERS
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."LOGIN_OFFICERS" ADD CONSTRAINT "FK_LOGIN_OFFICER" FOREIGN KEY ("EMP_ID")
	  REFERENCES "FUADRAFID"."OFFICER" ("EMP_ID") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table MAINTAINANCE
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."MAINTAINANCE" ADD CONSTRAINT "FK_MC_REG_NO" FOREIGN KEY ("REG_NO")
	  REFERENCES "FUADRAFID"."VEHICLE" ("REG_NO") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table MAINTAINANCE_COST
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."MAINTAINANCE_COST" ADD CONSTRAINT "FK_MC_RECEIPT_NO" FOREIGN KEY ("RECEIPT_NO")
	  REFERENCES "FUADRAFID"."EXPENSE" ("RECEIPT_NO") ENABLE;
  ALTER TABLE "FUADRAFID"."MAINTAINANCE_COST" ADD CONSTRAINT "FK_MC_SERIAL_NO" FOREIGN KEY ("SERIAL_NO", "REG_NO")
	  REFERENCES "FUADRAFID"."MAINTAINANCE" ("SERIAL_NO", "REG_NO") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table MEMBER_DEPOSITIONS
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."MEMBER_DEPOSITIONS" ADD CONSTRAINT "FK_MEMBER_ID" FOREIGN KEY ("MEMBER_ID")
	  REFERENCES "FUADRAFID"."MEMBERS" ("MEMBER_ID") ON DELETE CASCADE ENABLE;
  ALTER TABLE "FUADRAFID"."MEMBER_DEPOSITIONS" ADD CONSTRAINT "FK_SERIAL_NO" FOREIGN KEY ("SERIAL_NO")
	  REFERENCES "FUADRAFID"."INCOME" ("SERIAL_NO") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table MEMBER_ON_VEHICLE
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."MEMBER_ON_VEHICLE" ADD CONSTRAINT "FK_MEM_VH" FOREIGN KEY ("MEMBER_ID")
	  REFERENCES "FUADRAFID"."MEMBERS" ("MEMBER_ID") ON DELETE CASCADE ENABLE;
  ALTER TABLE "FUADRAFID"."MEMBER_ON_VEHICLE" ADD CONSTRAINT "FK_VH_MEM" FOREIGN KEY ("REG_NO")
	  REFERENCES "FUADRAFID"."VEHICLE" ("REG_NO") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table OFFICER
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."OFFICER" ADD CONSTRAINT "FK_EMP_ID_OFF" FOREIGN KEY ("EMP_ID")
	  REFERENCES "FUADRAFID"."EMPLOYEE" ("EMP_ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table OFFICER_APPLICANTS
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."OFFICER_APPLICANTS" ADD CONSTRAINT "FK_APP_NO_OFF" FOREIGN KEY ("APPLICATION_NO")
	  REFERENCES "FUADRAFID"."JOB_APPLICANTS" ("APPLICATION_NO") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table PRIZE_WINNERS
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."PRIZE_WINNERS" ADD CONSTRAINT "FK_PROG_NO_PW" FOREIGN KEY ("PROG_NO")
	  REFERENCES "FUADRAFID"."CULTURAL_PROGRAM" ("PROG_NO") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table PROGRAM_EXPENSE
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."PROGRAM_EXPENSE" ADD CONSTRAINT "FK_PROG_NO" FOREIGN KEY ("PROG_NO")
	  REFERENCES "FUADRAFID"."CULTURAL_PROGRAM" ("PROG_NO") ENABLE;
  ALTER TABLE "FUADRAFID"."PROGRAM_EXPENSE" ADD CONSTRAINT "FK_RECEIPT_NO_PROG" FOREIGN KEY ("RECEIPT_NO")
	  REFERENCES "FUADRAFID"."EXPENSE" ("RECEIPT_NO") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table PUBLISHED_BY
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."PUBLISHED_BY" ADD CONSTRAINT "FK_P_REG_NO" FOREIGN KEY ("P_REG_NO")
	  REFERENCES "FUADRAFID"."PUBLISHER" ("P_REG_NO") ON DELETE CASCADE ENABLE;
  ALTER TABLE "FUADRAFID"."PUBLISHED_BY" ADD CONSTRAINT "PUBLISHED_BY_FK_ISBN" FOREIGN KEY ("ISBN_NO")
	  REFERENCES "FUADRAFID"."BOOKS" ("ISBN_NO") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table REQUESTS_FOR
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."REQUESTS_FOR" ADD CONSTRAINT "REQUESTS_FOR_FK1" FOREIGN KEY ("MEMBER_ID")
	  REFERENCES "FUADRAFID"."MEMBERS" ("MEMBER_ID") ON DELETE CASCADE ENABLE;
  ALTER TABLE "FUADRAFID"."REQUESTS_FOR" ADD CONSTRAINT "REQUESTS_FOR_FK3" FOREIGN KEY ("ISBN_NO")
	  REFERENCES "FUADRAFID"."BOOKS" ("ISBN_NO") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table SALARY_GIVEN_TO
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."SALARY_GIVEN_TO" ADD CONSTRAINT "FK_EMP_ID_EMP" FOREIGN KEY ("EMP_ID")
	  REFERENCES "FUADRAFID"."EMPLOYEE" ("EMP_ID") ENABLE;
  ALTER TABLE "FUADRAFID"."SALARY_GIVEN_TO" ADD CONSTRAINT "FK_RECEIPT_NO_SAL" FOREIGN KEY ("RECEIPT_NO")
	  REFERENCES "FUADRAFID"."EXPENSE" ("RECEIPT_NO") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table SCHEDULING
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."SCHEDULING" ADD CONSTRAINT "FK_REG_NO_SCH" FOREIGN KEY ("REG_NO")
	  REFERENCES "FUADRAFID"."VEHICLE" ("REG_NO") ON DELETE CASCADE ENABLE;
  ALTER TABLE "FUADRAFID"."SCHEDULING" ADD CONSTRAINT "FK_SCHEDULE_NO" FOREIGN KEY ("SCHEDULE_NO")
	  REFERENCES "FUADRAFID"."SCHEDULE" ("SCHEDULE_NO") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table SUPPLIED_BY
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."SUPPLIED_BY" ADD CONSTRAINT "FK_STORE_ID_SUP" FOREIGN KEY ("STORE_ID")
	  REFERENCES "FUADRAFID"."BOOK_STORE" ("STORE_ID") ON DELETE CASCADE ENABLE;
  ALTER TABLE "FUADRAFID"."SUPPLIED_BY" ADD CONSTRAINT "SUPPLIED_BY_FK1" FOREIGN KEY ("ISBN_NO")
	  REFERENCES "FUADRAFID"."BOOKS" ("ISBN_NO") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table VEHICLE_BOUGHT
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."VEHICLE_BOUGHT" ADD CONSTRAINT "FK_RECEIPT_NO" FOREIGN KEY ("RECEIPT_NO")
	  REFERENCES "FUADRAFID"."EXPENSE" ("RECEIPT_NO") ON DELETE CASCADE ENABLE;
  ALTER TABLE "FUADRAFID"."VEHICLE_BOUGHT" ADD CONSTRAINT "FK_REG_NO" FOREIGN KEY ("REG_NO")
	  REFERENCES "FUADRAFID"."VEHICLE" ("REG_NO") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table VEHICLE_COMPONENTS
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."VEHICLE_COMPONENTS" ADD CONSTRAINT "FK_REG_NO_VC" FOREIGN KEY ("REG_NO")
	  REFERENCES "FUADRAFID"."VEHICLE" ("REG_NO") ON DELETE CASCADE ENABLE;
  ALTER TABLE "FUADRAFID"."VEHICLE_COMPONENTS" ADD CONSTRAINT "FK_SERIAL_NO_VC" FOREIGN KEY ("SERIAL_NO")
	  REFERENCES "FUADRAFID"."COMPONENTS" ("SERIAL_NO") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table WRITTEN_BY
--------------------------------------------------------

  ALTER TABLE "FUADRAFID"."WRITTEN_BY" ADD CONSTRAINT "FK_AUTHOR_ID" FOREIGN KEY ("AUTHOR_ID")
	  REFERENCES "FUADRAFID"."AUTHOR" ("AUTHOR_ID") ON DELETE CASCADE ENABLE;
  ALTER TABLE "FUADRAFID"."WRITTEN_BY" ADD CONSTRAINT "WRITTEN_BY_FK1" FOREIGN KEY ("ISBN_NO")
	  REFERENCES "FUADRAFID"."BOOKS" ("ISBN_NO") ENABLE;
