/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import javax.swing.table.DefaultTableModel;

/**
 *
 * @author FuadRafid
 */
public class Table_view extends javax.swing.JFrame {

    /**
     * Creates new form MaintananceLib
     */
    public Table_view() {
        initComponents();
        jTable1.setDefaultEditor(Object.class,null);
        
        
        
    }
    public void Schedule()
    {
        DataBase.ExecuteQuery("Select SCHEDULE_NO,DAY,ARRIVAL_TIME,DEPARTURE_TIME,DIVISION,DISTRICT,POLICE_STATION,STREET from fuadrafid.schedule natural join fuadrafid.scheduling  where  reg_no='"+Librarian.reg_no+"'", Login.con);
        Show_Users_In_JTable();
    }
    public void AllBooks()
    {
        DataBase.ExecuteQuery("Select isbn_no,books_name\"book\",category,edition,price,author from ALL_BOOK_IN_VEH where  reg_no='"+Librarian.reg_no+"'", Login.con);
        Show_Users_In_JTable();
    
    }
    public void Show_maintainance()
    {
        DataBase.ExecuteQuery("Select \"RECEIPT_NO\",\"DESCRIPTION\", \"DATE_DONE\", \"NEXT_DUE_DATE\",\"AMOUNT\",\"EXPENSE_ENTRY_DATE\"  from lib_maint where ID='"+Login.ID+"'", Login.con);
        Show_Users_In_JTable();
    }
     public void Show_Components()
    {
        DataBase.ExecuteQuery("Select SERIAL_NO,DESCRIPTION,STATUS,DATE_OF_ADDITION,TYPE  from LIB_VEH_COMP where reg_no='"+Librarian.reg_no+"'", Login.con);
        Show_Users_In_JTable();
    }
     public void Show_Users_In_JTable()
   {
       
       
      DefaultTableModel model = (DefaultTableModel)jTable1.getModel();
      model.setRowCount(0);
      model.setColumnCount(0);
      for (int i = 0; i < DataBase.TableHeader.size(); i++) 
       {
           model.addColumn(DataBase.TableHeader.elementAt(i));
       }
       for(int i = 0; i < DataBase.data.size(); i++)
       {     
           model.addRow(DataBase.data.elementAt(i));
           
       }

     TableColumnAdjuster ad=new TableColumnAdjuster(jTable1);
     ad.adjustColumns();   

   }
     public void book_author_view(String authId)
     {
         DataBase.ExecuteQuery("SELECT \"Book Name\",\"Category\",\"Edition\",\"Price\",\"Type\", \"Author\" FROM BOOK_AUTHOR_VIEW WHERE \"Exist\"=1 and \"AuthID\"='"+authId+"'",Login.con);
        Show_Users_In_JTable();
     }
     public void BOOK_PUBLISHER_VIEW(String pubId)
     {
        DataBase.ExecuteQuery("SELECT \"Book Name\",\"Category\",\"Edition\",\"Price\",\"Type\",\"Publisher\" FROM BOOK_PUBLISHER_VIEW WHERE \"Pub_id\"='"+pubId+"'",Login.con);
        Show_Users_In_JTable();
         
     }
      public void BookReq() 
      {
        DataBase.ExecuteQuery("SELECT book,author,edition from book_reqs "
                + " WHERE ID='"+Login.ID+"'",Login.con);
        Show_Users_In_JTable();
      }
      public void Officer_book_req()
      {
        DataBase.ExecuteQuery("SELECT \"BOOK\", \"AUTHOR\", \"EDITION\", \"CATEGORY\" from OFFICER_BOOK_REQSs "
                + " WHERE \"REG NO\"='"+Officer.reg_no+"'",Login.con);
        Show_Users_In_JTable();
      }
      public void BOOK_BOOKSTORE_VIEW(String storeId)
      {
        DataBase.ExecuteQuery("SELECT \"Book Name\",\"Category\",\"Edition\",\"Price\",\"Type\",\"Bought From\" FROM BOOK_BOOKSTORE_VIEW WHERE \"Store_ID\"='"+storeId+"'",Login.con);
        Show_Users_In_JTable();
      }
      
     public void loadIssueData() {
       DataBase.ExecuteQuery("Select books_name\"Book\",issue_date\"Date of Issue\",expiry_date\"Expiry Date\""
                + ",return_date\"Return Date\",member_name\"Member\" from fuadrafid.books "
                + "natural join fuadrafid.has_issue natural join fuadrafid.issue natural join fuadrafid.issued_to "
               + "natural join fuadrafid.members natural join fuadrafid.member_on_vehicle where "
                + "reg_no='"+Librarian.reg_no+"' order by issue_date desc" , Login.con);
        System.out.println(DataBase.data);
        Show_Users_In_JTable();
        DataBase.ExecuteQuery("Select books_name\"Book\",issue_date\"Date of Issue\",expiry_date\"Expiry Date\""
                + ",return_date\"Return Date\",member_name\"Member\" from fuadrafid.books "
                + "natural join fuadrafid.had_issue natural join fuadrafid.issue natural join fuadrafid.issued_to "
               + "natural join fuadrafid.members natural join fuadrafid.member_on_vehicle where "
                + "reg_no='"+Librarian.reg_no+"'  order by issue_date desc", Login.con);
        Add_more_data_to_table();
     jTable1.setDefaultEditor(Object.class, null);
    }
     
      private void Add_more_data_to_table() {
      DefaultTableModel model = (DefaultTableModel)jTable1.getModel();
       for(int i = 0; i < DataBase.data.size(); i++)
       {     
           model.addRow(DataBase.data.elementAt(i));
           
       }
     jTable1.setEnabled(false);
     jTable1.setColumnSelectionAllowed(false);
     jTable1.setRowSelectionAllowed(false);
     TableColumnAdjuster ad=new TableColumnAdjuster(jTable1);
     ad.adjustColumns();     
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jTable1.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_ALL_COLUMNS);
        jScrollPane1.setViewportView(jTable1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 648, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 593, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
   

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    // End of variables declaration//GEN-END:variables

    void PRIZE_WINNER_VIEW(String ProgID) {
        DataBase.ExecuteQuery("SELECT \"Winner Name\",\"Contact No.\",\"Category\", \"Position\" FROM PRIZE_WINNER_VIEW WHERE \"Employee In Charge\"='"+Login.ID+"' and \"Prog No\"='"+ProgID+"'",Login.con);
        Show_Users_In_JTable();
    }
}
