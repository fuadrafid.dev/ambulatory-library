/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

/**
 *
 * @author FuadRafid
 */
public class TableView extends javax.swing.JFrame implements TableModelListener{
    static Connection con=null;
    String id;
    /**
     * Creates new form TableVeiw
     */
    public TableView() {
        initComponents();
        jTable1.getModel().addTableModelListener(this);
        setExtendedState(java.awt.Frame.MAXIMIZED_BOTH);
        jDateChooser0.setDateFormatString("dd-mm-yyyy");
 
       
        for(int i=0;i<DataBase.Tables.size();i++)
        {
            choice1.add(DataBase.Tables.get(i));
        }
        
        Show_Users_In_JTable();
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
        @Override
        public void mouseClicked(java.awt.event.MouseEvent evt) {
        int row = jTable1.rowAtPoint(evt.getPoint());
        int col = jTable1.columnAtPoint(evt.getPoint());
        if (row >= 0 && col >= 0) {
                TableModel model = (TableModel)jTable1.getModel();
                id=(String) model.getValueAt(row, 0);
                System.out.println(id);    
        }
        }  
        });
        //TableModel datePicker = new DatePickerTable("01011999","12302000");;
        
    }
    
    
      public void Show_Users_In_JTable()
   {
       
       
      DefaultTableModel model = (DefaultTableModel)jTable1.getModel();
      model.setRowCount(0);
      model.setColumnCount(0);
      for (int i = 0; i < DataBase.TableHeader.size(); i++) 
       {
           model.addColumn(DataBase.TableHeader.elementAt(i));
       }
       for(int i = 0; i < DataBase.data.size(); i++)
       {     
           model.addRow(DataBase.data.elementAt(i));
           
       }
       
       try{
       jLabel2.setText("");jLabel3.setText("");jLabel4.setText("");jLabel5.setText("");
       jLabel6.setText("");jLabel7.setText("");jLabel8.setText("");jLabel9.setText("");
       jLabel10.setText("");jLabel11.setText("");
       jTextField2.setVisible(false);jTextField3.setVisible(false);jTextField4.setVisible(false);jTextField5.setVisible(false);
       jTextField6.setVisible(false);jTextField7.setVisible(false);jTextField8.setVisible(false);jTextField9.setVisible(false);
       jTextField10.setVisible(false);jTextField11.setVisible(false);
//       jDateChooser2.setVisible(false);jDateChooser3.setVisible(false);jDateChooser4.setVisible(false);jDateChooser5.setVisible(false);
//       jDateChooser6.setVisible(false);jDateChooser7.setVisible(false);jDateChooser8.setVisible(false);jDateChooser9.setVisible(false);
//       jDateChooser10.setVisible(false);jDateChooser11.setVisible(false);
       jLabel2.setText(DataBase.TableHeader.elementAt(0));
       jTextField2.setVisible(true);
       
       jLabel3.setText(DataBase.TableHeader.elementAt(1)); 
       jTextField3.setVisible(true);
       jLabel4.setText(DataBase.TableHeader.elementAt(2));
       jTextField4.setVisible(true);
       jLabel5.setText(DataBase.TableHeader.elementAt(3));
       jTextField5.setVisible(true);
       jLabel6.setText(DataBase.TableHeader.elementAt(4));
       jTextField6.setVisible(true);
       jLabel7.setText(DataBase.TableHeader.elementAt(5));
       jTextField7.setVisible(true);
       jLabel8.setText(DataBase.TableHeader.elementAt(6));
       jTextField8.setVisible(true);
       jLabel9.setText(DataBase.TableHeader.elementAt(7));
       jTextField9.setVisible(true);
       jLabel10.setText(DataBase.TableHeader.elementAt(8));
       jTextField10.setVisible(true);
       jLabel11.setText(DataBase.TableHeader.elementAt(9));
       jTextField11.setVisible(true);
       }
       catch(Exception e){}
       
       }
      private void clearEntry()
      {
       jTextField2.setText("");
       jTextField3.setText("");
       jTextField4.setText("");
       jTextField5.setText("");
       jTextField6.setText("");
       jTextField7.setText("");
       jTextField8.setText("");
       jTextField9.setText("");
       jTextField10.setText("");
       jTextField11.setText("");
      }
      public void tableChanged(TableModelEvent e) {
         
         try{
        int row = e.getFirstRow();
        int column = e.getColumn();
        TableModel model = (TableModel)e.getSource();
        String columnName = model.getColumnName(column);
        String data =(String) model.getValueAt(row, column);
        if(DataBase.colData.getColumnType(column+1)==Types.TIMESTAMP)
        {
            data="to_date('"+data+"','dd-mm-yyyy')";
        }
        else if(DataBase.colData.getColumnType(column+1)==Types.NUMERIC){}
        else
        {
            data="'"+data+"'";
        }
            
        String query="UPDATE "+choice1.getSelectedItem()+" SET "+columnName+" = "+data+" where "+
                DataBase.TableHeader.elementAt(0)+"="+"'"+id+"'";
        DataBase.ExecuteQuery(query, con);
        jButton1.doClick();
          System.out.println(query);
        }
        catch(Exception ex){}
    }
 

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        choice1 = new java.awt.Choice();
        jTextField2 = new javax.swing.JTextField();
        jTextField3 = new javax.swing.JTextField();
        jTextField4 = new javax.swing.JTextField();
        jTextField5 = new javax.swing.JTextField();
        jTextField6 = new javax.swing.JTextField();
        jTextField7 = new javax.swing.JTextField();
        jTextField8 = new javax.swing.JTextField();
        jTextField9 = new javax.swing.JTextField();
        jTextField10 = new javax.swing.JTextField();
        jTextField11 = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jDateChooser1 = new com.toedter.calendar.JDateChooser();
        jDateChooser2 = new com.toedter.calendar.JDateChooser();
        jDateChooser0 = new com.toedter.calendar.JDateChooser();
        jDateChooser4 = new com.toedter.calendar.JDateChooser();
        jDateChooser5 = new com.toedter.calendar.JDateChooser();
        jDateChooser3 = new com.toedter.calendar.JDateChooser();
        jDateChooser7 = new com.toedter.calendar.JDateChooser();
        jDateChooser9 = new com.toedter.calendar.JDateChooser();
        jDateChooser6 = new com.toedter.calendar.JDateChooser();
        jDateChooser8 = new com.toedter.calendar.JDateChooser();
        jLabel13 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("AMBULATORY LIBRARY MANAGEMENT SOFTWARE");
        setExtendedState(6);
        setFont(new java.awt.Font("Arial Unicode MS", 0, 10)); // NOI18N
        setMinimumSize(new java.awt.Dimension(1920, 1080));

        jTable1.setFont(new java.awt.Font("Arial Unicode MS", 0, 14)); // NOI18N
        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(jTable1);

        jButton1.setText("Show Table");
        jButton1.setActionCommand("");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel1.setText(" Choose Table");

        jLabel2.setText("jLabel2");

        jLabel3.setText("jLabel3");

        jLabel4.setText("jLabel4");

        jLabel5.setText("jLabel5");

        jLabel6.setText("jLabel6");

        jLabel7.setText("jLabel7");

        jLabel8.setText("jLabel8");

        jLabel9.setText("jLabel9");

        jLabel10.setText("jLabel10");

        jLabel11.setText("jLabel11");

        jButton2.setText("Insert");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jTextField2.setFont(new java.awt.Font("Arial Unicode MS", 0, 14)); // NOI18N
        jTextField2.setMaximumSize(new java.awt.Dimension(184, 30));
        jTextField2.setMinimumSize(new java.awt.Dimension(184, 30));
        jTextField2.setPreferredSize(new java.awt.Dimension(184, 30));

        jTextField3.setFont(new java.awt.Font("Arial Unicode MS", 0, 14)); // NOI18N
        jTextField3.setMaximumSize(new java.awt.Dimension(184, 30));
        jTextField3.setMinimumSize(new java.awt.Dimension(184, 30));
        jTextField3.setPreferredSize(new java.awt.Dimension(184, 30));

        jTextField4.setFont(new java.awt.Font("Arial Unicode MS", 0, 14)); // NOI18N
        jTextField4.setMaximumSize(new java.awt.Dimension(184, 30));
        jTextField4.setMinimumSize(new java.awt.Dimension(184, 30));
        jTextField4.setPreferredSize(new java.awt.Dimension(184, 30));
        jTextField4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField4ActionPerformed(evt);
            }
        });

        jTextField5.setFont(new java.awt.Font("Arial Unicode MS", 0, 14)); // NOI18N
        jTextField5.setMaximumSize(new java.awt.Dimension(184, 30));
        jTextField5.setMinimumSize(new java.awt.Dimension(184, 30));
        jTextField5.setPreferredSize(new java.awt.Dimension(184, 30));

        jTextField6.setFont(new java.awt.Font("Arial Unicode MS", 0, 14)); // NOI18N
        jTextField6.setMaximumSize(new java.awt.Dimension(184, 30));
        jTextField6.setMinimumSize(new java.awt.Dimension(184, 30));
        jTextField6.setPreferredSize(new java.awt.Dimension(184, 30));

        jTextField7.setFont(new java.awt.Font("Arial Unicode MS", 0, 14)); // NOI18N
        jTextField7.setMaximumSize(new java.awt.Dimension(184, 30));
        jTextField7.setMinimumSize(new java.awt.Dimension(184, 30));
        jTextField7.setPreferredSize(new java.awt.Dimension(184, 30));

        jTextField8.setFont(new java.awt.Font("Arial Unicode MS", 0, 14)); // NOI18N
        jTextField8.setMaximumSize(new java.awt.Dimension(184, 30));
        jTextField8.setMinimumSize(new java.awt.Dimension(184, 30));
        jTextField8.setPreferredSize(new java.awt.Dimension(184, 30));

        jTextField9.setFont(new java.awt.Font("Arial Unicode MS", 0, 14)); // NOI18N
        jTextField9.setMaximumSize(new java.awt.Dimension(184, 30));
        jTextField9.setMinimumSize(new java.awt.Dimension(184, 30));
        jTextField9.setPreferredSize(new java.awt.Dimension(184, 30));

        jTextField10.setFont(new java.awt.Font("Arial Unicode MS", 0, 14)); // NOI18N
        jTextField10.setMaximumSize(new java.awt.Dimension(184, 30));
        jTextField10.setMinimumSize(new java.awt.Dimension(184, 30));
        jTextField10.setPreferredSize(new java.awt.Dimension(184, 30));

        jTextField11.setFont(new java.awt.Font("Arial Unicode MS", 0, 14)); // NOI18N
        jTextField11.setMaximumSize(new java.awt.Dimension(184, 30));
        jTextField11.setMinimumSize(new java.awt.Dimension(184, 30));
        jTextField11.setPreferredSize(new java.awt.Dimension(184, 30));

        jLabel12.setFont(new java.awt.Font("Arial Unicode MS", 1, 36)); // NOI18N
        jLabel12.setText("Table Name");

        jDateChooser1.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jDateChooser1PropertyChange(evt);
            }
        });

        jDateChooser0.setDateFormatString("dd-mm-yyyy");
        jDateChooser0.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jDateChooser0PropertyChange(evt);
            }
        });
        jDateChooser0.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jDateChooser0KeyPressed(evt);
            }
        });

        jLabel13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/database/Bishwo-Shahitto-Kendro-logo1.png"))); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(277, 277, 277)
                        .addComponent(jLabel1)
                        .addGap(18, 18, 18)
                        .addComponent(choice1, javax.swing.GroupLayout.PREFERRED_SIZE, 238, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton1))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(33, 33, 33)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 1259, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 230, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jButton2)
                        .addGap(110, 110, 110))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel13)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel10, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel11, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel9, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel6, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.LEADING))
                                .addGap(48, 48, 48)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jTextField3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jTextField4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jTextField5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jTextField6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jTextField7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jTextField8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jTextField9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jTextField10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jTextField11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jDateChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jDateChooser2, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jDateChooser0, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                                    .addComponent(jDateChooser3, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jDateChooser4, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jDateChooser5, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jDateChooser7, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jDateChooser9, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jDateChooser6, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jDateChooser8, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(75, 75, 75))))
            .addGroup(layout.createSequentialGroup()
                .addGap(451, 451, 451)
                .addComponent(jLabel12)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(41, 41, 41)
                .addComponent(jLabel13)
                .addGap(43, 43, 43)
                .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(66, 66, 66)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(606, 606, 606)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(choice1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton1)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 547, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel2)
                                .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jDateChooser0, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(31, 31, 31)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel3)
                                .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jDateChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(31, 31, 31)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel4)
                                .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jDateChooser2, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(31, 31, 31)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel5)
                                .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jDateChooser3, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(31, 31, 31)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel6)
                                .addComponent(jTextField6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jDateChooser4, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(31, 31, 31)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jTextField7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel7))
                            .addComponent(jDateChooser5, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(31, 31, 31)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jTextField8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel8))
                            .addComponent(jDateChooser6, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(31, 31, 31)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jTextField9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel9))
                            .addComponent(jDateChooser7, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(31, 31, 31)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jTextField10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel10))
                            .addComponent(jDateChooser8, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(31, 31, 31)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jTextField11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel11))
                            .addComponent(jDateChooser9, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(55, 55, 55)
                        .addComponent(jButton2)))
                .addContainerGap(99, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        String query=choice1.getSelectedItem();
        query="Select * from "+query;
        DataBase.ExecuteQuery(query, con);
        Show_Users_In_JTable();
        System.out.println(query);
        jLabel12.setText(choice1.getSelectedItem());
        clearEntry();
    }//GEN-LAST:event_jButton1ActionPerformed
    
    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        String entry[]=new String [11];
        entry[1]=jTextField2.getText();
        entry[2]=jTextField3.getText();
        entry[3]=jTextField4.getText();
        entry[4]=jTextField5.getText();
        entry[5]=jTextField6.getText();
        entry[6]=jTextField7.getText();
        entry[7]=jTextField8.getText();
        entry[8]=jTextField9.getText();
        entry[9]=jTextField10.getText();
        entry[10]=jTextField11.getText();
        
        
        
        String qry=null;
        try {
            qry = "Insert into "+DataBase.colData.getTableName(1)+" values(";
        } catch (SQLException ex) {
            Logger.getLogger(TableView.class.getName()).log(Level.SEVERE, null, ex);
        }
        for(int i=1;i<=DataBase.colNo;i++)
        {
            String extra;
            try {
                if(DataBase.colData.getColumnType(i)==Types.TIMESTAMP)
                {
                    extra=",to_date('"+entry[i]+"','dd-mm-yyyy')";
                    if(i==1)
                        extra=extra.replaceFirst(",", "");
                    qry=qry+extra;
                }
               
                else if(DataBase.colData.getColumnType(i)==Types.NUMERIC)
                        {
                          
                          extra= ","+entry[i];;
                            if(i==1)
                        extra=extra.replaceFirst(",", "");
                            qry=qry+extra;
                        }
                else{
                    extra=",'"+entry[i]+"'";
                            if(i==1)
                        extra=extra.replaceFirst(",", "");
                    qry=qry+extra;
                }
                  
                System.out.println(DataBase.colData.getColumnType(i));
                System.out.println(DataBase.colData.getColumnTypeName(i));
            } catch (SQLException ex) {
                Logger.getLogger(TableView.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
         qry=qry+")";
        System.out.println(qry);
        DataBase.ExecuteQuery(qry, con);
        DataBase.ExecuteQuery("Select * from "+choice1.getSelectedItem(), con);
        Show_Users_In_JTable();
        
        
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jTextField4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField4ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField4ActionPerformed

    private void jDateChooser0KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jDateChooser0KeyPressed
       jTextField2.setText(jDateChooser0.toString());
    }//GEN-LAST:event_jDateChooser0KeyPressed

    private void jDateChooser0PropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jDateChooser0PropertyChange
        try{
        SimpleDateFormat sd= new SimpleDateFormat("dd-mm-yyyy");
        String date=sd.format(jDateChooser0.getDate());
        jTextField2.setText(date);
        }
        catch(Exception ex){}
    }//GEN-LAST:event_jDateChooser0PropertyChange

    private void jDateChooser1PropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jDateChooser1PropertyChange
        // TODO add your handling code here:
    }//GEN-LAST:event_jDateChooser1PropertyChange

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TableView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TableView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TableView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TableView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                con=DataBase.connectorDB();
                new TableView().setVisible(true);
             
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private java.awt.Choice choice1;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private com.toedter.calendar.JDateChooser jDateChooser0;
    private com.toedter.calendar.JDateChooser jDateChooser1;
    private com.toedter.calendar.JDateChooser jDateChooser2;
    private com.toedter.calendar.JDateChooser jDateChooser3;
    private com.toedter.calendar.JDateChooser jDateChooser4;
    private com.toedter.calendar.JDateChooser jDateChooser5;
    private com.toedter.calendar.JDateChooser jDateChooser6;
    private com.toedter.calendar.JDateChooser jDateChooser7;
    private com.toedter.calendar.JDateChooser jDateChooser8;
    private com.toedter.calendar.JDateChooser jDateChooser9;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField jTextField10;
    private javax.swing.JTextField jTextField11;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JTextField jTextField3;
    private javax.swing.JTextField jTextField4;
    private javax.swing.JTextField jTextField5;
    private javax.swing.JTextField jTextField6;
    private javax.swing.JTextField jTextField7;
    private javax.swing.JTextField jTextField8;
    private javax.swing.JTextField jTextField9;
    // End of variables declaration//GEN-END:variables
    private TableColumnAdjuster adjuster;
}
