/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Vector;

/**
 *
 * @author FuadRafid
 */
public class DataBase {
    public static Vector<String> TableHeader=new Vector<String>();
    public static Vector<Vector<String>> data=new Vector<Vector<String>>();
    public static ArrayList<String> Tables=new ArrayList<String>();
    public static ResultSetMetaData colData;
    public static int colNo=0;
    public static int DBA=0;
    public static Connection connectorDB()
    {
        Connection con=null;
        
     try
     {
         Class.forName("oracle.jdbc.OracleDriver");
         con=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE","fuadrafid","muhtasim");
        PreparedStatement stmt;
         ResultSet rs;
         ResultSetMetaData meta;
         stmt = con.prepareStatement("Select table_name from user_tables");
         rs=stmt.executeQuery(); 
         meta=rs.getMetaData();
         int tableNo=meta.getColumnCount();
         rs.next();
         while(rs.next())
         {
             Tables.add(rs.getString(1));
         }
         Collections.sort(Tables);
            
     }
     catch(ClassNotFoundException | SQLException e)
     {
         System.out.println(e);
     }
     
     return con;
        
    }
    public static Connection connectorDB(String Username, String Password)
    {
        Connection con=null;
        
     try
     {
         Class.forName("oracle.jdbc.OracleDriver");
         con=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE",Username,Password);
        PreparedStatement stmt;
//         ResultSet rs;
//         ResultSetMetaData meta;
//         stmt = con.prepareStatement("Select table_name from user_tables");
//         rs=stmt.executeQuery(); 
//         meta=rs.getMetaData();
//         int tableNo=meta.getColumnCount();
//         rs.next();
//         while(rs.next())
//         {
//             Tables.add(rs.getString(1));
//         }
//         Collections.sort(Tables);
            
     }
     catch(ClassNotFoundException | SQLException e)
     {
         System.out.println(e);
     }
     
     return con;
        
    }
    public static void ExecuteQuery(String s,Connection con)
    {
        
        System.out.println(s);
        data.clear();
        TableHeader.clear();
        try{
        PreparedStatement stmt; 
        stmt = con.prepareStatement(s);
         ResultSet rs=stmt.executeQuery(); 
         if(s.toLowerCase().contains("insert")||s.toLowerCase().contains("update")
                 ||s.toLowerCase().contains("drop")||s.toLowerCase().contains("create")||s.toLowerCase().contains("delete"))
             return;
         ResultSetMetaData meta=rs.getMetaData();
         colData=meta;
         colNo=meta.getColumnCount();
         int colno=meta.getColumnCount();
         for(int i=1;i<=colno;i++)
         {TableHeader.add(meta.getColumnName(i));}
         //System.out.println();
            while(rs.next()) 
            { 
              Vector<String> RowData=new Vector<String>();
        for(int i=1;i<=colno;i++)
        {
            String currData;
            Date date;
            if(meta.getColumnType(i)==93)
            {
                date=rs.getDate(i);
                if(date!=null)
                {
                SimpleDateFormat sd= new SimpleDateFormat("dd-LLL-yyyy");
                currData=sd.format(date);
                RowData.add(currData);
                continue;
                }
                else
                {
                currData=null;
                }
                
            }
            currData=rs.getString(i);
            RowData.add(currData);
        }
            data.add(RowData);
         //   System.out.println(data);
           
            }
       // System.out.println(data);
        System.out.println("Successful Run!");
        if(DBA==0)
          stmt.close();
        }
        catch(Exception e){System.out.println(e+"Data null");}
       
    }
    
   
  
}
    